package com.ijm.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

import com.ijm.dao.CasesDao;
import com.ijm.dao.EmailDao;
import com.ijm.dao.NameListDao;
import com.ijm.dao.UserDao;
import com.ijm.model.User;

public class Util {

	private static Util instance = null;
	private static Connection connection = null;
	private static Properties prop = null;

	private Util() {
		prop = new Properties();
		InputStream inputStream = Util.class.getClassLoader()
				.getResourceAsStream("config.properties");
		try {
			prop.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Util getInstance() {
		if (instance == null) {
			instance = new Util();
		}
		return instance;
	}

	public Connection getConnection() {
		try {
			if (connection != null && connection.isValid(0)) {
				return connection;
			}
			else {
					String driver = prop.getProperty("driver");
					String url = prop.getProperty("dburl");
					String user = prop.getProperty("user");
					String password = prop.getProperty("password");
					Class.forName(driver);
					connection = DriverManager.getConnection(url, user, password);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}
	
	/**
	 * 6-Apr-2016
	 * This method kills the connection object. This was done because after all the tables are populated, the getFilteredCases()
	 * does not fetch the CourtInfo's max(case_date). As the same connection object was used to populate and fetch data.
	 * Setting different transaction level did not help.
	 */
	public void destroyConnection() {
		try {
			//connection.abort(null);
			if (connection != null)
				connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			connection = null;
		}
	}
	
	public String getTextFileName() {
		String homeFolder = prop.getProperty("textfile");
		return homeFolder;
	}
	
	public String getHomeFolder() {
		String homeFolder = prop.getProperty("home_folder");
		return homeFolder;
	}

	public String getCaseUrl() {
		String caseurl = prop.getProperty("caseurl");
		//String[] caseurls = caseurl.split(";");
		return caseurl;
	}
	
	public String getCaseUrlFileExt() {
		String caseurl = prop.getProperty("caseurl_file_ext");
		//String[] caseurls = caseurl.split(";");
		return caseurl;
	}
	
	public String getCaseUrlPrefix() {
		String prefix = prop.getProperty("caseurl_prefix");
		return prefix;
	}
	
	public String getCaseUrlMonthlyPrefix() {
		String prefix = prop.getProperty("caseurl_monthly_prefix");
		return prefix;
	}
	

	public String getCaseNameListFile() {
		String emailList = prop.getProperty("casename_list_file");
		return emailList;
	}

	/*public String getCaseDetailsFile() {
		String emailList = prop.getProperty("casedetails_file");
		return emailList;
	}*/

	public String getSchedule() {
		String schedule = prop.getProperty("schedule");
		return schedule;
	}
	
	public String getArchiveFolder() {
		String archiveFolder = prop.getProperty("archive_folder");
		if (archiveFolder == null)
			archiveFolder = "";
		return archiveFolder;
	}
	
	public String getArchiveFileTemplate() {
		String archiveFileTemplate = prop.getProperty("archive_file_template");
		return archiveFileTemplate;
	}
	
	/*public String getArchiveFileName() {
		String archiveFileName = prop.getProperty("archive_file_name");
		return archiveFileName;
	}*/	

	public String getEmailListFile() {
		String emailList = prop.getProperty("email_list_file");
		return emailList;
	}

	public String getEmailFrom() {
		String emailFrom = prop.getProperty("email_from");
		return emailFrom;
	}

	public String getEmailUsername() {
		String emailUsername = prop.getProperty("email_username");
		return emailUsername;
	}

	public String getEmailPassword() {
		String emailPassword = prop.getProperty("email_password");
		return emailPassword;
	}

	public String getEmailSmtpHost() {
		String emailSmtpHost = prop.getProperty("email_smtpHost");
		return emailSmtpHost;
	}

	public String getEmailSmtpPort() {
		String emailSmtpPort = prop.getProperty("email_smtpPort");
		return emailSmtpPort;
	}
	
	/**
	 * Return sendEmail status. Which can be modified in config.properties
	 * @return true or false
	 */
	public boolean getSendEmailStatus() {
		String status = prop.getProperty("send_email");
		boolean sendEmail = Boolean.parseBoolean( status );
		return sendEmail;
	}
	
	public boolean getDownloadPdfStatus() {
		boolean status = Boolean.parseBoolean( prop.getProperty("download_pdf") );
		return status;
	}
	
	private int getCauselistDataAvailabilitySearchDays() {
		int count = Integer.parseInt( prop.getProperty("causelist_data_availability_search_days") );
		return count;
	}

	public boolean validateUser(String username, String password) {
		UserDao userDao = new UserDao();
		User user = userDao.getUser(username);
		String passwordEncrypted = encrypt(password);
		
		boolean status = false;
		if (user != null)
			status = passwordEncrypted.equals(user.getPassword());
		
		return status;
	}

	public String encrypt(String msg) {
		StringBuffer sb = null;
		try {
			sb = new StringBuffer();
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] passBytes = msg.getBytes();
			md.reset();
			byte[] digested = md.digest(passBytes);
			for (int i = 0; i < digested.length; i++) {
				sb.append(Integer.toHexString(0xff & digested[i]));
			}
			//System.out.println(" Cryptic - source:"+msg+" \t result:" + sb.toString());
		} catch (NoSuchAlgorithmException e) {
			System.out.println("Encryption error: " + e);
		}
		return sb.toString();
	}

	public void readURL() {
		try {
			File f = new File(prop.getProperty("caseurl"));

			OutputStream oos = new FileOutputStream("C:/avisek/personal/workspace/projects/IJM/test/test.pdf");

			byte[] buf = new byte[8192];
			
			InputStream is = new FileInputStream(f);
			
			/*BufferedReader contentReader = null;
			contentReader = new BufferedReader(new InputStreamReader(is));
			
			String inputLine;
			while ((inputLine = contentReader.readLine()) != null) {
				System.out.println(inputLine);
			}*/

			int c = 0;
			int count = 0;
			while ((c = is.read(buf, 0, buf.length)) > 0) {
				oos.write(buf, 0, c);
				oos.flush();
				System.out.println(buf.length);
				count++;
				//System.out.println(buf);
				//String bytesAsString = new String(buf, StandardCharsets.UTF_8);
				//System.out.println(bytesAsString);
			}
			System.out.println("\n\t count="+count);
			oos.close();
			System.out.println("stop");
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public boolean downloadPDF() {
		
		boolean isMonthly = false;
		
		InputStream in = null;
		String fileName = "";
		//Date today = new Date();
		Calendar calendar = Calendar.getInstance();
		Date dtMax = new CasesDao().getMaxDate();
		calendar.setTime(dtMax);
		for (int i=0; i < getCauselistDataAvailabilitySearchDays(); i++) {
			try {
				try {
					Date dateUtil = calendar.getTime();
					//System.out.println("present date :"+dateUtil);
					calendar.add(Calendar.DAY_OF_MONTH, 1);
					dateUtil = calendar.getTime();
					//System.out.println("next date :"+dateUtil);
					
					java.sql.Date dateSql = new java.sql.Date(dateUtil.getTime());
					//System.out.println("sqlDate :"+dateSql);
					String year = dateSql.toString().substring(0, 4);
					String month = dateUtil.toString().substring(4, 7);
					String dd = dateSql.toString().substring(8);
					String mm = dateSql.toString().substring(5, 7);
					
					//for First Monday of Every Month
					if(calendar.get(Calendar.DAY_OF_MONTH) <= 7 && calendar.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
						fileName = getCaseUrlMonthlyPrefix()+dd+mm+year;
						isMonthly = true;
					}
					else {
						fileName = getCaseUrlPrefix()+dd+mm+year;
					}
					
					String pdf = year+"/"+month+"/"+fileName;
					//System.out.println(pdf);
					
					//if (true) return;
					//URL url = new URL("http://clists.nic.in/ddir/PDFCauselists/calcutta/2015/Oct/00316102015.pdf");
					URL url = new URL(getCaseUrl()+pdf+getCaseUrlFileExt());
					in = url.openStream();
					System.out.println("URL: "+getCaseUrl()+pdf+getCaseUrlFileExt());
				} catch (FileNotFoundException e) {
					System.out.println(" \n\n File not found Error-----------------\n" + e);
					continue;
				} 
				Files.copy(in, Paths.get(getHomeFolder()+fileName+".pdf"), StandardCopyOption.REPLACE_EXISTING);
				System.out.println("Download successful.... to "+getHomeFolder()+fileName+".pdf");
				in.close();
				pdfToText(getHomeFolder()+fileName+".pdf");
				break;
			} catch (Exception e) {
				System.out.println(" \n\n Error------ in downloadPDF() --- \n" + e);
			}
		}
		return isMonthly;
	}
	
	private String pdfToText(String fileName) {
		PDFParser parser;
		//StringBuilder parsedText = new StringBuilder();
		PDFTextStripper pdfStripper = null;
		PDDocument pdDoc = null;
		COSDocument cosDoc = null;
		File file = new File(fileName);
		if (!file.isFile()) {
			System.err.println("File " + fileName + " does not exist.");
			return null;
		}
		try {
            //String text = "Your sample content to save in a text file.";
            BufferedWriter out = new BufferedWriter(new FileWriter(getHomeFolder()+getTextFileName()));
			//OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(getHomeFolder()+getTextFileName()), Charset.forName("UTF-8"));
			
            try {
    			parser = new PDFParser(new FileInputStream(file));
    		} catch (IOException e) {
    			System.err.println("Unable to open PDF Parser. " + e.getMessage());
    			out.close();
    			return null;
    		}
    		try {
    			parser.parse();
    			cosDoc = parser.getDocument();
    			pdfStripper = new PDFTextStripper();
    			//System.out.println("\nPDF StartPage:"+pdfStripper.getStartPage()+"\t\t EndPage:"+pdfStripper.getEndPage()+"\n\n");
    			pdDoc = new PDDocument(cosDoc);
    			pdfStripper.setStartPage(pdfStripper.getStartPage());
    			pdfStripper.setEndPage(pdfStripper.getEndPage());
    			
    			out.write(pdfStripper.getText(pdDoc));
    			//parsedText.append(pdfStripper.getText(pdDoc));
    			//System.out.println(parsedText);
    		} catch (Exception e) {
    			System.err
    					.println("An exception occured in parsing the PDF Document."
    							+ e.getMessage());
    		} finally {
    			try {
    				if (cosDoc != null)
    					cosDoc.close();
    				if (pdDoc != null)
    					pdDoc.close();
    			} catch (Exception e) {
    				e.printStackTrace();
    			}
    		}
    		//System.out.println("\n PDF parsed successfully having "+parsedText.length()+" no. of characters.\n");
            
            out.close();
        }
        catch (IOException e)
        {
            System.out.println("Exception in pdfToText().... "+e);       
        }
		
		//return parsedText.toString();
		return "";
	}
	
	public static void main(String z[]) throws SQLException {
		//NameListDao dao = new NameListDao();
		//dao.deleteAllNames();
		//dao.addNames();
		
		//EmailDao emailDao = new EmailDao();
		//emailDao.addEmails();
		
		//String crypted = Util.getInstance().encrypt("abc");
		//System.out.println(" Cryptic - source:abc \t result:" + crypted);
		Util.getInstance().downloadPDF();
		//String s = Util.getInstance().pdfToText("00322032016.pdf");
		//System.out.println("################ \n"+s);
		
		//int n = Util.getInstance().getConnection().getTransactionIsolation();
		//System.out.println("Transaction level:"+n);
		
	}

}