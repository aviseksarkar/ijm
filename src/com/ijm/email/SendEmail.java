package com.ijm.email;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.ijm.dao.EmailDao;
import com.ijm.util.Util;

public class SendEmail {

	String to = "";
	String from = null;
	String username = null;
	String password = null;
	String smtpHost = null;
	String smtpPort = null;
	boolean sendEmail = false;
	Properties props = null;
	
	public static SendEmail instance = null;
	
	/*public static SendEmail getInstance() {
		if (instance == null)
			instance = new SendEmail();
		return instance;
	}*/

	public SendEmail() {
		
		EmailDao emailDao = new EmailDao();
		List<String> listNames = emailDao.getAllEmails();
        for ( String name : listNames) {
            to += name+",";
        }
        to = to.trim().substring(0, to.lastIndexOf(','));
        
		from = Util.getInstance().getEmailFrom();
		username = Util.getInstance().getEmailUsername();
		password = Util.getInstance().getEmailPassword();
		
		smtpHost = Util.getInstance().getEmailSmtpHost();
		smtpPort = Util.getInstance().getEmailSmtpPort();
		sendEmail = Util.getInstance().getSendEmailStatus();
		props = new Properties();
		
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", smtpHost);
		props.put("mail.smtp.port", smtpPort);
	}
	
	public void sendEmail(String emailText, String date) {
		
	      // Get the Session object.
	      Session session = Session.getInstance(props,
	      new javax.mail.Authenticator() {
	         protected PasswordAuthentication getPasswordAuthentication() {
	            return new PasswordAuthentication(username, password);
	         }
	      });

	      try {
	         // Create a default MimeMessage object.
	         Message message = new MimeMessage(session);

	         // Set From: header field of the header.
	         message.setFrom(new InternetAddress(from));

	         // Set To: header field of the header.
	         message.setRecipients(Message.RecipientType.TO,
	         InternetAddress.parse(to));

	         // Set Subject: header field
	         message.setSubject("IJM Case Results - "+date);

	         // Now set the actual message
	         //message.setText("Hello, this is sample for to check send email using JavaMailAPI ");
	         //message.setText(emailText);
	         
	         MimeMessage   msg = new MimeMessage(session);
	         MimeMultipart mpart = new MimeMultipart();
	         MimeBodyPart bp = new MimeBodyPart();
	         //bp.setText("plain text and html text like<b>Test</>", CHARSET_UTF_8, MESSAGE_HTML_CONTENT_TYPE);
	         // add message body
	         mpart.addBodyPart(bp);

	         // At last setting multipart In MimeMessage
	         message.setContent(emailText, "text/html");
	         
	         // Send message
	         Transport.send(message);
	         
	         Date dt = Calendar.getInstance().getTime();
	         System.out.println(dt+" : Sent Email successfully....");

	      } catch (MessagingException e) {
	          //throw new RuntimeException(e);
	    	  System.out.println("Error Sending email: "+e+"\n");
	    	  e.printStackTrace();
	      }
	}
	
   /*public static void main(String[] args) {
	   new SendEmail().sendEmail("Calling from SendEmail - Main()", "Now");
   }*/
}


