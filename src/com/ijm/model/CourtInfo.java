package com.ijm.model;

import java.sql.Date;
import java.util.List;

public class CourtInfo {
	
	private String justiceName;
	private Date date;
	private String courtNumber;
	private String notes;
	private List<Case> cases;
	
	public CourtInfo() {}
	
	public CourtInfo(String courtNumber, Date date, String justiceName) {
		this.courtNumber = courtNumber;
		this.date = date;
		this.justiceName = justiceName;
	}
	
	public String getJusticeName() {
		return justiceName;
	}
	
	public void setJusticeName(String justiceName) {
		this.justiceName = justiceName;
	}
	
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public String getCourtNumber() {
		return courtNumber;
	}

	public void setCourtNumber(String courtNumber) {
		this.courtNumber = courtNumber;
	}
	
	public String getNotes() {
		return notes;
	}
	
	public void setNotes(String notes) {
		this.notes = notes;
	}

	public List<Case> getCases() {
		return cases;
	}

	public void setCases(List<Case> cases) {
		this.cases = cases;
	}
}
