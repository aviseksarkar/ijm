package com.ijm.model;

import java.sql.Date;


public class Case {

    private Date date;
    private String caseSerialNumber;
    private String caseNumber;
    private String partyName;
    private String caseAgainst;
    private String attorneyName;
    private String courtNumber;
    private String time;
    private String justiceName;    

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getCaseSerialNumber() {
		return caseSerialNumber;
	}

	public void setCaseSerialNumber(String caseSerialNumber) {
		this.caseSerialNumber = caseSerialNumber;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public String getCaseAgainst() {
		return caseAgainst;
	}

	public void setCaseAgainst(String caseAgainst) {
		this.caseAgainst = caseAgainst;
	}

	public String getAttorneyName() {
		return attorneyName;
	}

	public void setAttorneyName(String attorneyName) {
		this.attorneyName = attorneyName;
	}

	public String getCourtNumber() {
		return courtNumber;
	}

	public void setCourtNumber(String courtNumber) {
		this.courtNumber = courtNumber;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getJusticeName() {
		return justiceName;
	}

	public void setJusticeName(String justiceName) {
		this.justiceName = justiceName;
	}

	@Override
    public String toString() {
        return "Case [CaseSerialNo=" + caseSerialNumber + ", date=" + date
                + ", CasseNumber=" + caseNumber + ", Date=" + date + ", PartyName=" + partyName
                + ", CasseAgainst=" + caseAgainst + ", AttorneyName=" + attorneyName+ ", CourtNo="
                + courtNumber + ", Time=" + time + "]";
    }    
}
