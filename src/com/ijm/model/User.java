package com.ijm.model;

import java.sql.Date;

public class User {
	
	private String username;
	private String password;
	private String firstname;
	private String middlename;
	private String lastname;
	private Date dob;
	private String email;
	
	public String getUsername() {
		return username;
	}
	public String getPassword() {
		return password;
	}
	public String getFirstname() {
		return firstname;
	}
	public String getMiddlename() {
		return middlename;
	}
	public String getLastname() {
		return lastname;
	}
	public Date getDob() {
		return dob;
	}
	public String getEmail() {
		return email;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
