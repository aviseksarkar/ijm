package com.ijm.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.ijm.dao.NameListDao;
import com.ijm.domain.ScheduleJob;
import com.ijm.domain.URLParser;
import com.ijm.model.CourtInfo;
import com.ijm.model.User;
import com.ijm.util.Util;
import com.okta.saml.Application;
import com.okta.saml.Configuration;
import com.okta.saml.SAMLValidator;

@MultipartConfig
public class Controller2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static String INSERT_OR_EDIT = "/case.jsp";
	// private static String LIST_CASES_PAGE = "/listCases.jsp";
	private static String LIST_CASES_PAGE = "/index.jsp";
	private static String LOGIN_PAGE = "/login.jsp";
	private static String ADMIN_PAGE = "/admin.jsp";
	private static String COURTS_PAGE = "/courts.jsp";
	private static String CREATE_USER_PAGE = "/createuser.jsp";
	private static String CREATE_NAME_PAGE = "/name.jsp";
	private static String NAME_LIST_PAGE = "/namelist.jsp";
	private static String LOGIN_STATUS = "loginStatus";
	private static String LOGIN_ERROR_MSG = "<font color=red>Invalid UserName or Password. Please try again.</font>";
	private static String MANDATORY_FIELD_ERROR_MSG = "<font color=white><B><I>All (*) marked fields are mandatory.</I></B></font>";
	private static String PAGE_ERROR = "page_error";
	
	protected static final String SAML_REQUEST = "SAMLRequest";
    protected static final String SAML_RESPONSE = "SAMLResponse";
    protected static final String RELAY_STATE = "RelayState";
    protected SAMLValidator validator;
    protected Configuration configuration;
    protected Application app;
    
    int countServerCalling = 0;

	public Controller2() {
		super();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		//System.out.println("\n Controller2 ---- doGet ------\n");
				
		serve(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		//System.out.println("\n Controller2 ******* doPost *******\n");

		serve(request, response);
	}

	private void serve(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//System.out.println("\n Controller2 ******* serve() *******  " + countServerCalling++ +"\n");
		
		/*Enumeration<String> attributeNames = request.getAttributeNames();
		if (attributeNames.hasMoreElements()){
			String attrib = attributeNames.nextElement();
			System.out.println(" #-# "+attrib+"="+request.getParameter(attrib));
		}
		
		Enumeration<String> paramNames = request.getParameterNames();
		if (paramNames.hasMoreElements()){
			String param = paramNames.nextElement();
			System.out.println(" *-* "+param+"="+request.getParameter(param));
		}*/
		
		
		NameListDao nameListDao = new NameListDao();
		String forward = "";
		String command = request.getParameter("command");
		String nameOriginal = request.getParameter("nameOriginal");
		String nameModify = request.getParameter("nameModify");
		String nameAdd = request.getParameter("nameAdd");
		//System.out.println("command: "+command+" \t nameOriginal: "+nameOriginal+" \t nameModify: "+nameModify);
		
		if (command != null && command.equals("nameModify")) {
			nameListDao.updateName(nameOriginal, nameModify);
		} else if (command != null && command.equals("nameDelete")) {
			nameListDao.deleteName(nameOriginal);
		} else if (command != null && command.equals("nameAdd")) {
			nameListDao.addName(nameAdd);
		}
		
		List<String> names = nameListDao.getAllNames();
		request.setAttribute("names", names);
		forward = NAME_LIST_PAGE;
		
		RequestDispatcher view = request.getRequestDispatcher(forward);
		view.forward(request, response);
	}

	public void destroy() {
		System.out.println("IJMServlet2 shutting down....");
		Util.getInstance().destroyConnection();
		ScheduleJob.getInstance().stop();
	}
}
