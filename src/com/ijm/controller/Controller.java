package com.ijm.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.sql.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.opensaml.ws.security.SecurityPolicyException;

import com.ijm.dao.CasesDao;
import com.ijm.dao.UserDao;
import com.ijm.domain.ScheduleJob;
import com.ijm.domain.URLParser;
import com.ijm.model.CourtInfo;
import com.ijm.model.User;
import com.ijm.util.Util;
import com.okta.saml.Application;
import com.okta.saml.Configuration;
import com.okta.saml.SAMLRequest;
import com.okta.saml.SAMLResponse;
import com.okta.saml.SAMLValidator;

@MultipartConfig
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static String INSERT_OR_EDIT = "/case.jsp";
	// private static String LIST_CASES_PAGE = "/listCases.jsp";
	private static String LIST_CASES_PAGE = "/index.jsp";
	private static String LOGIN_PAGE = "/login.jsp";
	private static String ADMIN_PAGE = "/admin.jsp";
	private static String COURTS_PAGE = "/courts.jsp";
	private static String CREATE_USER_PAGE = "/createuser.jsp";
	private static String CREATE_NAME_PAGE = "/name.jsp";
	private static String NAME_LIST_PAGE = "/namelist.jsp";
	private static String LOGIN_STATUS = "loginStatus";
	private static String LOGIN_ERROR_MSG = "<font color=red>Invalid UserName or Password. Please try again.</font>";
	private static String MANDATORY_FIELD_ERROR_MSG = "<font color=white><B><I>All (*) marked fields are mandatory.</I></B></font>";
	private static String PAGE_ERROR = "page_error";
	
	protected static final String SAML_REQUEST = "SAMLRequest";
    protected static final String SAML_RESPONSE = "SAMLResponse";
    protected static final String RELAY_STATE = "RelayState";
    protected SAMLValidator validator;
    protected Configuration configuration;
    protected Application app;
    private static String entityID="https://ijm.okta.com/home/icims/0oa114w2jxzbQ0ewv0i8/695";

	public Controller() {
		super();
		//System.out.println("Conroller() ............ Before Schedule Job .... ");
		ScheduleJob.getInstance();
		URLParser.getInstance();
		//System.out.println("Conroller() ............ After URLParser.getInstance() invoke .... ");
		
		try {
            InputStream stream = getClass().getResourceAsStream("/valid-config.xml");
            String file;
            try {
                file = convertStreamToString(stream);
            } finally {
                stream.close();
            }
            //System.out.println("Conroller() ............ After reading from valid-config.xml to file .... ");
            
            validator = new SAMLValidator();

            // Load configuration from the xml for the template app
            configuration = validator.getConfiguration(file);
            System.out.println("Conroller() ............ After loading Configuration from valid-config.xml .... "+configuration);
            
            app = configuration.getDefaultApplication();
            System.out.println("Conroller() ............ DefaultApp is .... "+app);
            
            if (configuration.getDefaultEntityID() == null) {
            	System.out.println("Default application has not been configured in configuration.");
            } 
            if (app == null) {
            	System.out.println("Could not find default application in configuration: " + configuration.getDefaultEntityID());
            }
            app = configuration.getApplication(entityID);
            System.out.println("Conroller() ............ App with EntityID .... "+app);
            if (app == null) {
            	System.out.println("Could not find application in configuration for entityID: " + entityID);
            }
        } catch (Exception e) {
        	System.out.println("**** Error while validating SAML Configuration");
            e.printStackTrace();
        }
		//System.out.println("Conroller() ............ exiting .... ");
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			System.out.println("\n---- doGet ------\n");
			
			HttpSession session = request.getSession(false);
			System.out.println("session="+session+"\t isNew="+session.isNew()+"\t UserPrincipal"+request.getUserPrincipal());
			/*if(session != null) {
				Enumeration<String> attributeNames = session.getAttributeNames();
				while (attributeNames.hasMoreElements()){
					String attrib = attributeNames.nextElement();
					System.out.println(" ********** "+attrib+"="+request.getAttribute(attrib));
				}
			}*/
			
			Enumeration<String> attributeNames = request.getAttributeNames();
			System.out.println("\n - Attributes: ");
			while (attributeNames.hasMoreElements()){
				String attrib = attributeNames.nextElement();
				System.out.println(" #-# "+attrib+"="+request.getAttribute(attrib));
			}
			
			Enumeration<String> paramNames = request.getParameterNames();
			System.out.println("\n - Parameters: ");
			while (paramNames.hasMoreElements()){
				String param = paramNames.nextElement();
				System.out.println(" *-* "+param+"="+request.getParameter(param));
			}
			
			String user = request.getParameter("user");
			System.out.println("user="+user+"\n");
			String cert = request.getParameter("cert");
			System.out.println("cert="+cert+"\n");
			String username = request.getParameter("username");
			System.out.println("username="+username+"\n");
			
			//if (user != null) {
			if (! session.isNew()) {	//isNew() will return false when the response comes from Okta
				try {
		            /*String relay = request.getParameter(RELAY_STATE);
		            if (relay != null && !relay.isEmpty()) {
		                DeepLinkServlet deepLinkServlet = new DeepLinkServlet();
		                deepLinkServlet.doPost(request, response);
		                return;
		            }*/

		            String assertion = request.getParameter(SAML_RESPONSE);
		            if (assertion == null) {
		                throw new Exception("SAMLResponse parameter missing");
		            }
		            assertion = new String(Base64.decodeBase64(assertion.getBytes("UTF-8")), Charset.forName("UTF-8"));
		            System.out.println(assertion);

		            SAMLResponse samlResponse = validator.getSAMLResponse(assertion, configuration);
		            System.out.println("SAML authentication successful");
		            request.setAttribute("user", samlResponse.getUserID());
		            System.out.println("--- user="+samlResponse.getUserID());
		            
		            request.getRequestDispatcher("index.jsp").forward(request, response);
		        } catch (SecurityPolicyException e) {
		        	System.out.println("SAML authentication unsuccessful");
		            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		        }

				serve(request, response);
			} else {
				SAMLRequest samlRequest = validator.getSAMLRequest(app);
				// Base64 encode and then URL encode the SAMLRequest
				String encodedSaml = Base64.encodeBase64String(samlRequest.toString().getBytes());
				String url = app.getAuthenticationURL();
				url += "?" + SAML_REQUEST + "=" + URLEncoder.encode(encodedSaml, "UTF-8");
				System.out.println("Redirecting to: " + url);
				response.sendRedirect(url);
			}
        } catch (Exception e) {
        	System.out.println(e);
            e.printStackTrace();
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
		
		serve(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			System.out.println("\n******* doPost *******\n");
            /*String relay = request.getParameter(RELAY_STATE);
            if (relay != null && !relay.isEmpty()) {
                DeepLinkServlet deepLinkServlet = new DeepLinkServlet();
                deepLinkServlet.doPost(request, response);
                return;
            }*/

            String assertion = request.getParameter(SAML_RESPONSE);
            if (assertion == null) {
                throw new Exception("SAMLResponse parameter missing");
            }
            assertion = new String(Base64.decodeBase64(assertion.getBytes("UTF-8")), Charset.forName("UTF-8"));
            System.out.println(assertion);

            SAMLResponse samlResponse = validator.getSAMLResponse(assertion, configuration);
            System.out.println("SAML authentication successful");
            request.setAttribute("user", samlResponse.getUserID());
            System.out.println("--- user="+samlResponse.getUserID());
            request.getRequestDispatcher("index.jsp").forward(request, response);
        } catch (SecurityPolicyException e) {
        	System.out.println("SAML authentication unsuccessful");
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        } catch (Exception e) {
        	System.out.println(e);
            e.printStackTrace();
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
		
		serve(request, response);
	}

	private void serve(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		CasesDao casesDao = new CasesDao();
		UserDao userDao = new UserDao();
		List<CourtInfo> courts = null;	
		String forward = "";
		String action = request.getParameter("action");
		
		String courtDate = request.getParameter("courtdate");
		System.out.println(" -----  Request Parameter - courtdate: "+courtDate+"      action: "+action);
		if( courtDate != null ) {
			courts = casesDao.getAllCourtsByDate(courtDate);
			request.setAttribute("courts", courts);
			request.setAttribute("courtDate", courtDate);
			forward = COURTS_PAGE;
			
		} else if (action.equalsIgnoreCase("listCases")) {
			String command = request.getParameter("command");
			String username = request.getParameter("username");
			String password = request.getParameter("password");
			Boolean loginStatus = (Boolean) request.getSession(false).getAttribute(LOGIN_STATUS);
			
			//String newliSessionne = System.getProperty("line.separator");
			if (command != null && command.contains("\n")) {
				//System.out.println("++++ command = "+command);
				command = command.replace("\n", " ");
				//System.out.println("---- command = "+command+"\n");
			}
			
			System.out.println("LoginStatus : " + loginStatus + "\t command : "+ command);
			System.out.println("MultipartConfig:"+ServletFileUpload.isMultipartContent(request));
			if (ServletFileUpload.isMultipartContent(request) && command == null)//for multipart/form-data the field value becomes null
				command = "Validate";
			//System.out.println("--- " + (loginStatus != null && loginStatus));
			forward = LIST_CASES_PAGE;

			if (command != null && command.equalsIgnoreCase("logout")) {
				//request.getSession().removeAttribute(LOGIN_STATUS);
				request.getSession().invalidate();
				forward = LOGIN_PAGE;
				System.out.println("logout performed...");
			} else if (!(loginStatus != null && loginStatus)) { // If user not logged in then...
				if (command != null && command.equalsIgnoreCase("login")) {
					System.out.println("********** login *******  " + username+ " --------- " + password);
					if ((username != null && password != null)) {
						if (Util.getInstance().validateUser(username, password)) {
							if (username.equals("admin")) {
								System.out.println("********** Admin login **************");
								request.setAttribute(PAGE_ERROR, "");//Remove the LoginError
								request.getSession().setAttribute(LOGIN_STATUS,Boolean.TRUE);
								//UserDao userDao = new UserDao();
								List<User> users = userDao.getAllUsers();
								request.setAttribute("users", users);
								forward = ADMIN_PAGE;
							} else {
								System.out.println("********** User login **************");
								request.getSession().setAttribute(LOGIN_STATUS,Boolean.TRUE);
								request.setAttribute(PAGE_ERROR, "");//Remove the LoginError
								courts = casesDao.getAllFilteredCases();
								request.setAttribute("courts", courts);
								forward = LIST_CASES_PAGE;
							}
						} else {
							System.out.println("--------- InValid login ------------");
							request.setAttribute(PAGE_ERROR, LOGIN_ERROR_MSG);
							forward = LOGIN_PAGE;
						}
					} 
				} else { //for first time login
					System.out.println("--------- New login ------------");
					request.setAttribute(PAGE_ERROR, "");//Set the LoginError to blank
					forward = LOGIN_PAGE;
				}
			} else if (command != null && command.equalsIgnoreCase("displayuserform")) {//Display User Form
				forward = CREATE_USER_PAGE;
			} else if (command != null && command.equalsIgnoreCase("displaynamelistform")) {//Display User Form
				forward = NAME_LIST_PAGE;
				System.out.println("Controller - Display Create Name Form");
			} else if (command != null && command.equalsIgnoreCase("Create User")) {//Create User
				String fieldUsername = request.getParameter("username");
				String fieldPassWord = request.getParameter("password");
				String fieldFirstName = request.getParameter("firstname");
				String fieldMiddleName = request.getParameter("middlename");
				String fieldLastName = request.getParameter("lastname");
				String fieldDob = request.getParameter("dob");
				String fieldEmail = request.getParameter("email");
				if  ( (fieldUsername == null || fieldUsername.length()==0) || (fieldPassWord == null || fieldPassWord.length()==0) || 
						(fieldFirstName == null || fieldFirstName.length()==0) || (fieldLastName == null || fieldLastName.length()==0) ||
						(fieldDob == null || fieldDob.length()==0) || (fieldEmail == null || fieldEmail.length()==0) ) {

					request.setAttribute(PAGE_ERROR, MANDATORY_FIELD_ERROR_MSG);
					forward = CREATE_USER_PAGE;
				} else {
					User user = new User();
					user.setUsername(fieldUsername);
					user.setPassword(fieldPassWord);
					user.setFirstname(fieldFirstName);
					user.setMiddlename(fieldMiddleName);
					user.setLastname(fieldLastName);
					Date sqlDate = java.sql.Date.valueOf(fieldDob);
					user.setDob(sqlDate);
					//System.out.println("Dob : "+fieldDob);
					user.setEmail(fieldEmail);
					
					userDao.addUser(user);
					
					request.setAttribute(PAGE_ERROR, "");
					List<User> users = userDao.getAllUsers();
					request.setAttribute("users", users);
					forward = ADMIN_PAGE;
				}
				//System.out.println("Controller - User Creation");
			} else if (command != null && command.equalsIgnoreCase("Display Users")) {//Display Users
				List<User> users = userDao.getAllUsers();
				request.setAttribute("users", users);
				forward = ADMIN_PAGE;
			} else if (command != null && command.equalsIgnoreCase("List Cases")) {//List Cases for Admin
				courts = casesDao.getAllFilteredCases();
				request.setAttribute("courts", courts);
				forward = LIST_CASES_PAGE;
			} else if (command != null && command.equalsIgnoreCase("useredit")) {
				String fieldUsername = request.getParameter("username");
				User user = userDao.getUser(fieldUsername);
				request.setAttribute("user", user);
				forward = CREATE_USER_PAGE;
			} else if (command != null && command.equalsIgnoreCase("userdelete")) {
				String fieldUsername = request.getParameter("username");
				userDao.deleteUser(fieldUsername);
				List<User> users = userDao.getAllUsers();
				request.setAttribute("users", users);
				forward = ADMIN_PAGE;				
			} else if (command != null && command.equalsIgnoreCase("Validate")) { //Validate Archive File
				String content = validate(request);
				if (content != null) {
					courts = URLParser.getInstance().parse(content);
					request.setAttribute("courts", courts);
					forward = LIST_CASES_PAGE;
				} else {
					List<User> users = userDao.getAllUsers();
					request.setAttribute("users", users);
					forward = ADMIN_PAGE;
				}
			} else if (command != null && command.equalsIgnoreCase("search")) {
				//System.out.println("Controller - Search ");
				String partyName = request.getParameter("party");
				String attorneyName = request.getParameter("attorney");
				courts = casesDao.getFilteredCases(partyName, attorneyName);
				request.setAttribute("courts", courts);
			} else if (command != null && command.equalsIgnoreCase("history")) {
				//System.out.println("Controller - History ");
				String fromDate = request.getParameter("fromdate");
				String toDate = request.getParameter("todate");
				courts = casesDao.getFilteredCasesByDate(fromDate, toDate);
				request.setAttribute("courts", courts);
				//System.out.println("From: " + fromDate + "\t To:" + toDate);
			} else {
				//cases = casesDao.getAllFilteredCases();
				//request.setAttribute("cases", cases);
				//System.out.println("Else --------- New login ------------");
				request.getSession().invalidate();
				request.setAttribute(PAGE_ERROR, "");//Set the LoginError to blank
				forward = LOGIN_PAGE;
			}
		}
		System.out.println("forward="+forward);
		RequestDispatcher view = request.getRequestDispatcher(forward);
		view.forward(request, response);
	}

	private String validate(HttpServletRequest request) {
		String content = null;
		FileItem fileItem = null;
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		try {
			List<FileItem> fields = upload.parseRequest(request);
			Iterator<FileItem> it = fields.iterator();
			if (!it.hasNext()) {
				//System.out.println("No fields found");
				return null;
			}
			while (it.hasNext()) {
				fileItem = it.next();
				boolean isFormField = fileItem.isFormField();
				if (! isFormField) {
					break;
				}
			}
		} catch (FileUploadException e) {
			e.printStackTrace();
		}
		
	    BufferedReader reader = null;
	    try {
	    	reader = new BufferedReader(new InputStreamReader(fileItem.getInputStream()));
	        String inputLine;
	        StringBuilder sb = new StringBuilder();
	        while ((inputLine = reader.readLine()) != null) {
	        	sb.append(inputLine+"\n");
	        	//System.out.println("-" + inputLine);
	        }
	        content = sb.toString();
	    } catch (IOException e) {
	    	System.out.println("Problems during file upload. Error:" + e);
		} finally {
	        if (reader != null) {
	            try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
	        }
	    }
	    return content;
	}
	
	private static String convertStreamToString(InputStream is)
    {
        Scanner s = (new Scanner(is, "UTF-8")).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
	
	public void destroy() {
		System.out.println("IJMServlet shutting down....");
		Util.getInstance().destroyConnection();
		ScheduleJob.getInstance().stop();
	}
}
