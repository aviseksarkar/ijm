package com.ijm.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ijm.dao.EmailDao;
import com.ijm.domain.ScheduleJob;
import com.ijm.util.Util;

@MultipartConfig
public class Controller3 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static String INSERT_OR_EDIT = "/case.jsp";

	private static String EMAIL_LIST_PAGE = "/emaillist.jsp";
    
    int countServerCalling = 0;

	public Controller3() {
		super();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		//System.out.println("\n Controller2 ---- doGet ------\n");
				
		serve(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		//System.out.println("\n Controller2 ******* doPost *******\n");

		serve(request, response);
	}

	private void serve(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//System.out.println("\n Controller2 ******* serve() *******  " + countServerCalling++ +"\n");
		
		/*Enumeration<String> attributeNames = request.getAttributeNames();
		if (attributeNames.hasMoreElements()){
			String attrib = attributeNames.nextElement();
			System.out.println(" #-# "+attrib+"="+request.getParameter(attrib));
		}
		
		Enumeration<String> paramNames = request.getParameterNames();
		if (paramNames.hasMoreElements()){
			String param = paramNames.nextElement();
			System.out.println(" *-* "+param+"="+request.getParameter(param));
		}*/
		
		
		EmailDao emailDao = new EmailDao();
		String forward = "";
		String command = request.getParameter("command");
		String nameOriginal = request.getParameter("nameOriginal");
		String nameModify = request.getParameter("nameModify");
		String nameAdd = request.getParameter("nameAdd");
		//System.out.println("command: "+command+" \t nameOriginal: "+nameOriginal+" \t nameModify: "+nameModify);
		
		if (command != null && command.equals("nameModify")) {
			emailDao.updateEmail(nameOriginal, nameModify);
		} else if (command != null && command.equals("nameDelete")) {
			emailDao.deleteEmail(nameOriginal);
		} else if (command != null && command.equals("nameAdd")) {
			emailDao.addEmail(nameAdd);
		}
		
		List<String> names = emailDao.getAllEmails();
		request.setAttribute("names", names);
		forward = EMAIL_LIST_PAGE;
		
		RequestDispatcher view = request.getRequestDispatcher(forward);
		view.forward(request, response);
	}

	public void destroy() {
		System.out.println("IJMServlet3 shutting down....");
		Util.getInstance().destroyConnection();
		ScheduleJob.getInstance().stop();
	}
}
