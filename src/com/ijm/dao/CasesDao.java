package com.ijm.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import com.ijm.domain.URLParser;
import com.ijm.model.Case;
import com.ijm.model.CourtInfo;
import com.ijm.util.Util;

public class CasesDao {

    private Connection connection;

    public CasesDao() {
        connection = Util.getInstance().getConnection();
    }

    public void addCase(Case cas) {
        try {
            PreparedStatement preparedStatement = Util.getInstance().getConnection()
                    .prepareStatement("insert into cases(case_date,court_number,case_time,case_serial_no,case_number,party_name,case_against,attorney_name,justice_name) values (?, ?, ?, ?, ?, ?, ?, ?, ?)");
            //preparedStatement.setString(1, cas.getDate());
            preparedStatement.setDate(1, cas.getDate());
            preparedStatement.setString(2, cas.getCourtNumber());
            preparedStatement.setString(3, cas.getTime());
            preparedStatement.setString(4, cas.getCaseSerialNumber());
            preparedStatement.setString(5, cas.getCaseNumber());
            preparedStatement.setString(6, cas.getPartyName());
            preparedStatement.setString(7, cas.getCaseAgainst());
            preparedStatement.setString(8, cas.getAttorneyName());
            preparedStatement.setString(9, cas.getJusticeName());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void addCases(List<Case> cases) {
    	Case cas = null;
        try {
            PreparedStatement preparedStatement = Util.getInstance().getConnection()
                    .prepareStatement("insert into cases(case_date,court_number,case_time,case_serial_no,case_number,party_name,case_against,attorney_name,justice_name) values (?, ?, ?, ?, ?, ?, ?, ?, ?)");
            //connection.setAutoCommit(false);
            
            //for (cas : cases) {
            for ( int i=0; i < cases.size(); i++) {
            	cas = cases.get(i);
            	preparedStatement.setDate(1, cas.getDate());
	            preparedStatement.setString(2, cas.getCourtNumber());
	            preparedStatement.setString(3, cas.getTime());
	            preparedStatement.setString(4, cas.getCaseSerialNumber());
	            preparedStatement.setString(5, cas.getCaseNumber());
	            preparedStatement.setString(6, cas.getPartyName());
	            preparedStatement.setString(7, cas.getCaseAgainst());
	            preparedStatement.setString(8, cas.getAttorneyName()); 
	            preparedStatement.setString(9, cas.getJusticeName());
	            preparedStatement.addBatch();
            }
           	preparedStatement.executeBatch();
           	//connection.commit();
           	
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("**** \n CourtNo:"+cas.getCourtNumber()+"\t CaseSerialNo:"+cas.getCaseSerialNumber()+"\t CaseNumber:"+cas.getCaseNumber()+"\t PartyName:"+cas.getPartyName()+"\t CaseAgainst:"+cas.getCaseAgainst()+"\t AttorneyName:"+cas.getAttorneyName()+"\t JusticeName:"+cas.getJusticeName()+"\n **********");
        }
    }

    public void deleteCases() {
        try {
            PreparedStatement preparedStatement = Util.getInstance().getConnection().prepareStatement("delete from cases");
            //connection.setAutoCommit(false);
            preparedStatement.executeUpdate();
            //connection.commit();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void deleteTempCases() {
        try {
            PreparedStatement preparedStatement = Util.getInstance().getConnection().prepareStatement("delete from temp_cases");
            //connection.setAutoCommit(false);
            preparedStatement.executeUpdate();
            //connection.commit();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAliasCases() {
        try {
            PreparedStatement preparedStatement = Util.getInstance().getConnection().prepareStatement("delete from alias_cases");
            //connection.setAutoCommit(false);
            preparedStatement.executeUpdate();
            //connection.commit();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /*public void updateCase(Case cas) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("update users set firstname=?, lastname=?, dob=?, email=?" +
                            "where userid=?");
            // Parameters start with 1
            preparedStatement.setString(1, cas.getFirstName());
            preparedStatement.setString(2, cas.getLastName());
            preparedStatement.setDate(3, new java.sql.Date(cas.getDob().getTime()));
            preparedStatement.setString(4, cas.getEmail());
            preparedStatement.setInt(5, cas.getUserid());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }*/

    /**
     * Populates the FILTERED_CASES table with the matching names from name_list.
     * 
	   The following does the match with names beginning with the names given in the name_list2 file. Name_list2 contains names after removing
	   the spaces between the firstname and secondname etc.
     */
    public void populateFilteredCases() {
        try {
        	//The following will insert the data for entries matching with the names starting with.
        	//6-Apr-2016: Replaced CONCAT(b.name,'%') with CONCAT('%',b.name,'%') to incorporate the name search in the middle of the content
            PreparedStatement preparedStatement = Util.getInstance().getConnection()
                    .prepareStatement("INSERT INTO `filtered_cases`( `case_date`, `court_number`, `case_serial_no`,`case_number`,`party_name`,`case_against`,`attorney_name`,`justice_name`) "
                    		+ " (SELECT a.case_date, a.court_number, a.case_serial_no, a.case_number, c.party_name, c.case_against, c.attorney_name, a.justice_name "
                    		+ "FROM `temp_cases` a, `name_list2` b, `cases` c WHERE a.party_name like CONCAT('%',b.name,'%') AND a.case_number = c.case_number "
                    		+ "AND (a.case_number like ('%CRR%') OR a.case_number like ('%CRA%') OR a.case_number like ('%CRM%') OR a.case_number like ('%AST%')) "
                    		+ "GROUP BY a.case_number);");
            
            //The following will insert the data for entries with Alias names starting with.
            PreparedStatement preparedStatement2 = Util.getInstance().getConnection()
                    .prepareStatement("INSERT INTO `filtered_cases`( `case_date`, `court_number`, `case_serial_no`,`case_number`,`party_name`,`case_against`,`attorney_name`,`justice_name`) "
                            +"(SELECT a.case_date, a.court_number, a.case_serial_no, a.case_number, c.party_name, c.case_against, c.attorney_name, a.justice_name FROM `alias_cases` a, `name_list2` b, `cases` c WHERE a.party_name like CONCAT(b.name,'%') AND a.case_number = c.case_number AND "
                           +"(a.case_number like ('%CRR%') OR a.case_number like ('%CRA%') OR a.case_number like ('%CRM%') OR a.case_number like ('%AST%')) GROUP BY a.case_number);");
            
            /* The following will insert data for Deepak Prahladka cases in case it exists in case_against. For this, no case_type filters are needed after discussion with Edward on 17th June*/
            PreparedStatement preparedStatement3 = Util.getInstance().getConnection()
                    .prepareStatement("INSERT INTO `filtered_cases`( `case_date`, `court_number`, `case_serial_no`,`case_number`,`party_name`,`case_against`,`attorney_name`,`justice_name`) "+
                    		"(SELECT a.case_date, a.court_number, a.case_serial_no, a.case_number, c.party_name, c.case_against, c.attorney_name, a.justice_name FROM `temp_cases` a, `name_dipak` b, "+
                    		"`cases` c WHERE a.case_against like CONCAT(b.name,'%') AND a.case_number = c.case_number GROUP BY a.party_name);");
            
            /* The following will insert data for Deepak Prahladka cases in case it exists in attorney_name. For this, no case_type filters are needed after discussion with Edward on 17th June*/
            PreparedStatement preparedStatement4 = Util.getInstance().getConnection()
                    .prepareStatement("INSERT INTO `filtered_cases`( `case_date`, `court_number`, `case_serial_no`,`case_number`,`party_name`,`case_against`,`attorney_name`,`justice_name`) "+
                    		"(SELECT a.case_date, a.court_number, a.case_serial_no, a.case_number, c.party_name, c.case_against, c.attorney_name, a.justice_name FROM `temp_cases` a, `name_dipak` b, "+
                    		"`cases` c WHERE a.attorney_name like CONCAT(b.name,'%') AND a.case_number = c.case_number GROUP BY a.case_number);");
            
            /* The following will insert data for case_against also. This is following the conversation with Edward of 22nd June 2015
            that, sometimes the convict names (party_against) are also of interest to IJM */ 
            //6-Apr-2016: Replaced CONCAT(b.name,'%') with CONCAT('%',b.name,'%') to incorporate the name search in the middle of the content
            PreparedStatement preparedStatement5 = Util.getInstance().getConnection()
                    .prepareStatement("INSERT INTO `filtered_cases`( `case_date`, `court_number`, `case_serial_no`,`case_number`,`party_name`,`case_against`,`attorney_name`,`justice_name`) "+
                            "(SELECT a.case_date, a.court_number, a.case_serial_no, a.case_number, c.party_name, c.case_against, c.attorney_name, a.justice_name FROM `temp_cases` a, `name_list2` b, `cases` c WHERE a.case_against like CONCAT('%',b.name,'%') AND a.case_number = c.case_number AND "+ 
                            "(a.case_number like ('%CRR%') OR a.case_number like ('%CRA%') OR a.case_number like ('%CRM%') OR a.case_number like ('%AST%')) GROUP BY a.case_number);");
            
            PreparedStatement preparedStatement6 = Util.getInstance().getConnection()
                    .prepareStatement("INSERT INTO `filtered_cases`( `case_date`, `court_number`, `case_serial_no`,`case_number`,`party_name`,`case_against`,`attorney_name`,`justice_name`) (SELECT a.case_date, a.court_number, a.case_serial_no, a.case_number, c.party_name, c.case_against, c.attorney_name, a.justice_name FROM `temp_cases` a, `name_list` b, `cases` c WHERE a.case_number=CONCAT(b.name,'%') AND a.case_number = c.case_number GROUP BY a.case_number);");
            
            //connection.setAutoCommit(false);
            preparedStatement.executeUpdate();
            preparedStatement2.executeUpdate();
            preparedStatement3.executeUpdate();
            preparedStatement4.executeUpdate();
            preparedStatement5.executeUpdate();
            preparedStatement6.executeUpdate();
            //connection.commit();
            //System.out.println("------- populateFilteredCase() ------------- ");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    
    public void populateTempCases() {
    	/*We have to modify the temp_cases to have attorney names and case_against also without spaces.

    	This is to deal with the special cases of Deepak Prahladka, entries for which should be present

    	irrespective of presence in party_name or case_against or attorney_name */
    	// 06-04-2016 : Additional replacement of space for case_number 
        try {
            PreparedStatement preparedStatement = Util.getInstance().getConnection()
                    .prepareStatement("INSERT INTO temp_cases (`case_date`, `court_number`, `case_serial_no`,`case_number`,`party_name`,`case_against`,`attorney_name`,`justice_name`) "+
                    		"(SELECT case_date,court_number,case_serial_no,case_number, REPLACE(party_name,' ',''),REPLACE(case_against,' ',''),"+
                    		"REPLACE(attorney_name,' ',''), justice_name from cases);");
            //connection.setAutoCommit(false);
            preparedStatement.executeUpdate();
            //connection.commit();
            //System.out.println("------- populateFilteredCase() ------------- ");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void populateAliasCases() {
        try {
            PreparedStatement preparedStatement = Util.getInstance().getConnection()
                    .prepareStatement("INSERT INTO alias_cases (`case_date`, `court_number`, `case_serial_no`, `case_number`,`party_name`,`case_against`,`attorney_name`,`justice_name`) "+
                    		"(SELECT case_date,court_number, case_serial_no,case_number, "+
                    			"REPLACE(party_name,LEFT(party_name,locate(\"ALIAS\",party_name)+4),''),"+
                    				"case_against,attorney_name,justice_name from temp_cases where party_name like '%ALIAS%') ;");
            //connection.setAutoCommit(false);
            preparedStatement.executeUpdate();
            //connection.commit();
            //System.out.println("------- populateFilteredCase() ------------- ");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /*public List<CourtInfo> getAllCases() {
    	List<CourtInfo> courts = new ArrayList<CourtInfo>();
		String query = "select * from courts";
		courts = getCourtsFromQuery(query);
    	return courts;
    }*/
    
    /*public List<CourtInfo> getAllFilteredCases() {
        List<CourtInfo> courts = new ArrayList<CourtInfo>();
        String query = "select * from court_info where case_date in (select max(case_date) from court_info)";
        courts = getCourtAndCasesFromQuery(query);
        return courts;
    }*/
    
    public List<CourtInfo> getAllFilteredCases() {
        List<CourtInfo> courts = new ArrayList<CourtInfo>();
        String query = "select * from court_info where case_date in (select max(case_date) from court_info)";
        courts = getCourtsFromQuery(query);
        for( CourtInfo court : courts) {
        	query = "select * from filtered_cases where case_date='"+ court.getDate()+"' and court_number="+ court.getCourtNumber() +" and justice_name='"+ court.getJusticeName() +"';";
        	List<Case> cases = new ArrayList<Case>();
        	try {
        		Statement statement = Util.getInstance().getConnection().createStatement();
        		ResultSet rs = statement.executeQuery(query);
    			while (rs.next()) {
    				Case cas = new Case();
    				Date date = rs.getDate("case_date");
    				cas.setDate(date);
    				String courtNumber = rs.getString("court_number");
    				cas.setCourtNumber(courtNumber);
    				cas.setTime(rs.getString("case_time"));
    				cas.setCaseSerialNumber(rs.getString("case_serial_no"));
    				cas.setCaseNumber(rs.getString("case_number"));
    				cas.setPartyName(rs.getString("party_name"));
    				cas.setCaseAgainst(rs.getString("case_against"));
    				cas.setAttorneyName(rs.getString("attorney_name"));
    				cas.setJusticeName(rs.getString("justice_name"));
    				cases.add(cas);
    			}
    			court.setCases(cases);
    			
    		} catch (SQLException e) {
    			e.printStackTrace();
    		}
        }
        return courts;
    }
    
    public List<CourtInfo> getFilteredCases(String partyName, String attorneyName) {
    	List<CourtInfo> courts = new ArrayList<CourtInfo>();
    	List<Case> cases = new ArrayList<Case>();
        String query = "SELECT * FROM filtered_cases WHERE party_name LIKE CONCAT('%','"+partyName+"','%')"+
            					" AND attorney_name LIKE CONCAT('%','"+attorneyName+"','%') group by party_name";
        cases = getCasesFromQuery(query);
        //System.out.println("query-1 : "+query);
        //System.out.println("Filtered Cases Number by PartyAttorney : "+cases.size());
        
        //find the uniqueness of the court details
        HashSet<String> courtDetailsSet = new HashSet<String>();
        for(Case cas : cases){
        	courtDetailsSet.add(cas.getCourtNumber()+":"+cas.getDate().toString()+":"+cas.getJusticeName());
        }
        //System.out.println("Unique court details:"+courtDetailsSet.size());
        
        //create the courts list
        Iterator<String> itr = courtDetailsSet.iterator();
        while (itr.hasNext()) {
        	String courtDetails[] = itr.next().split(":");
        	//CourtInfo court = new CourtInfo(courtDetails[0], java.sql.Date.valueOf(courtDetails[1]), courtDetails[2]);
        	query = "select * from court_info where court_number='"+courtDetails[0]+
        			"' AND case_date='"+courtDetails[1]+"' AND justice_name='"+courtDetails[2]+"' group by case_date";
        	//System.out.println("query-2 : "+query);
        	List<CourtInfo> tmpCourts = getCourtsFromQuery(query);
        	courts.addAll(tmpCourts);
        	//System.out.println("Temp Courts:"+tmpCourts.size()+"\t"+query);
        	//courts.add(court);
        }
        //System.out.println("All Courts in the filtered search:"+courts.size());
        
        //populate the courts list with cases
        for(Case cas : cases){
        	for(CourtInfo court : courts ){ 
        		if( court.getCourtNumber().equals(cas.getCourtNumber()) && court.getDate().equals(cas.getDate()) && 
        				court.getJusticeName().equals(cas.getJusticeName())) {
        			if (court.getCases() != null) {
        				court.getCases().add(cas);
        			}
        			else {
        				List<Case> tempCases = new ArrayList<Case>(1);
        				tempCases.add(cas);
        				court.setCases(tempCases);
        			}
        			break;
        		}
        	}
        }
        
        //courts = getCourtAndCasesFromQuery(query);
        return courts;
    }
    
    public List<CourtInfo> getFilteredCasesByDate(String fromDate, String toDate) {
        List<CourtInfo> courts = new ArrayList<CourtInfo>();
        String query = "select * from court_info where case_date between '"+fromDate+"' AND '"+toDate+"'";
        courts = getCourtAndCasesFromQuery(query);
        return courts;
    }

    private List<Case> getCasesFromQuery(String query) {
    	List<Case> cases = new ArrayList<Case>();
    	HashSet<String> setCourtNo = new HashSet<String>();
    	HashSet<String> setDate = new HashSet<String>();
    	try {
    		Statement statement = Util.getInstance().getConnection().createStatement();
    		ResultSet rs = statement.executeQuery(query);
			while (rs.next()) {
				Case cas = new Case();
				Date date = rs.getDate("case_date");
				cas.setDate(date);
				String courtNumber = rs.getString("court_number");
				cas.setCourtNumber(courtNumber);
				cas.setTime(rs.getString("case_time"));
				cas.setCaseSerialNumber(rs.getString("case_serial_no"));
				cas.setCaseNumber(rs.getString("case_number"));
				cas.setPartyName(rs.getString("party_name"));
				cas.setCaseAgainst(rs.getString("case_against"));
				cas.setAttorneyName(rs.getString("attorney_name"));
				cas.setJusticeName(rs.getString("justice_name"));
				cases.add(cas);
				//Colon (:) separated court and date value for uniqueness in CourtInfo table
				setCourtNo.add(courtNumber);
				setDate.add(date.toString());
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	
    	return cases;
    }
    
    public Date getMaxDate() {
        Date date = null;
        try {
        	//if (connection == null )	System.out.println("\n\n\n\t\t !!! Connection NULL !!! \n\n\n");
            Statement statement = Util.getInstance().getConnection().createStatement();
            //ResultSet rs = statement.executeQuery("select max(case_date) from filtered_cases");
            //Modified as on 6-Jun-2016 as no data saved in filtered_cases causing infinite loop of downloading the same PDF
            ResultSet rs = statement.executeQuery("select max(case_date) from cases");
            while (rs.next()) {
                date = rs.getDate(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return date;
    }
    
    public void addCourts(List<CourtInfo> courts) {
    	CourtInfo court = null;
        try {
            PreparedStatement preparedStatement = Util.getInstance().getConnection()
                    .prepareStatement("insert into court_info(court_number,justice_name,case_date,notes) values (?, ?, ?, ?)");
            //connection.setAutoCommit(false);
            
            for ( int i=0; i < courts.size(); i++) {
            	court = courts.get(i);
            	preparedStatement.setString(1, court.getCourtNumber());
            	preparedStatement.setString(2, court.getJusticeName());
            	preparedStatement.setDate(3, court.getDate());
	            preparedStatement.setString(4, court.getNotes());
	            preparedStatement.addBatch();
	            //System.out.println("CourtNo:"+court.getCourtNumber()+"  notes.length="+court.getNotes().length()+"  cases.size="+court.getCases().size());
            }
           	preparedStatement.executeBatch();
           	//connection.commit();
           	
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("**** \n CourtNumber:"+court.getCourtNumber()+"\t CourtDate:"+court.getDate());
        }
    }

    public List<CourtInfo> getAllCourtsByDate(String date) {
        List<CourtInfo> courts = new ArrayList<CourtInfo>();
        String query = "select * from court_info where case_date='"+date+"'";
        courts = getCourtAndCasesFromQuery(query);
        return courts;
    }
    
    private List<CourtInfo> getCourtsFromQuery(String query) {
        List<CourtInfo> courts = new ArrayList<CourtInfo>();
        try {
    		Statement statement = Util.getInstance().getConnection().createStatement();
    		ResultSet rs = statement.executeQuery(query);
    		while (rs.next()) {
            	CourtInfo court = new CourtInfo();
                court.setDate(rs.getDate("case_date"));
                court.setCourtNumber(rs.getString("court_number"));
                court.setJusticeName(rs.getString("justice_name"));
                court.setNotes(rs.getString("notes"));
                courts.add(court);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return courts;
    }
    
    private List<CourtInfo> getCourtAndCasesFromQuery(String query) {
    	//List<Case> cases = new ArrayList<Case>();
        List<CourtInfo> courts = new ArrayList<CourtInfo>();
        List<CourtInfo> courtsFound = new ArrayList<CourtInfo>();
        courts = getCourtsFromQuery(query);
        //int caseCount = 0;
        for (CourtInfo court : courts) {
        	//Added group by to remove duplicate records as on 22-06-2015
        	query = "select * from filtered_cases where case_date='"+court.getDate().toString()+"' AND court_number='"+court.getCourtNumber()+"' AND justice_name='"+court.getJusticeName()+"' group by party_name";
        	List<Case> cases = getCasesFromQuery(query);
        	if ( cases.size() > 0 ){
        		court.setCases(cases);
        		courtsFound.add(court);
        		/*System.out.println("Query "+query);
        		System.out.println("CourtNo.="+ court.getCourtNumber()+"		Cases="+cases.size());
        		caseCount += cases.size();*/
        	}
        }
        /*System.out.println("- Total Cases="+caseCount);
        query = "select * from filtered_cases where case_date in (select max(case_date) from filtered_cases)";
        cases = getCasesFromQuery(query);
        System.out.println(" Total Cases="+cases.size());*/
        
        return courtsFound;
    }
    
    public static void main(String z[]) {
    	CasesDao dao = new CasesDao();
    	List<CourtInfo> courts = dao.getAllFilteredCases();
    	//URLParser.getInstance().sendEmail(courts);
    	//dao.getFilteredCasesByDate("2015-01-19", "2015-01-22");
    	//dao.getFilteredCases("", "das");
    }
    
}