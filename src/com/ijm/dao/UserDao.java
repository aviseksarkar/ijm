package com.ijm.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.ijm.model.Case;
import com.ijm.model.CourtInfo;
import com.ijm.model.User;
import com.ijm.util.Util;

public class UserDao {

    private Connection connection;

    public UserDao() {
        connection = Util.getInstance().getConnection();
    }

    public void addUser(User user) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("insert into user(username, password, firstname, middlename, lastname, dob, email) values(?,?,?,?,?,?,?)");
            //connection.setAutoCommit(false);
            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, Util.getInstance().encrypt(user.getPassword()));
            preparedStatement.setString(3, user.getFirstname());
            preparedStatement.setString(4, user.getMiddlename());
            preparedStatement.setString(5, user.getLastname());
            preparedStatement.setDate(6, user.getDob());
            preparedStatement.setString(7, user.getEmail());
            preparedStatement.executeUpdate();
            //connection.commit();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void addUsers(List<User> users) {
    	User user = null;
        try {
        	PreparedStatement preparedStatement = connection
                    .prepareStatement("insert into user(username, password, firstname, middlename, lastname, dob, email) values(?,?,?,?,?,?,?)");            
        	//connection.setAutoCommit(false);
            
            for ( int i=0; i < users.size(); i++) {
            	user = users.get(i);
            	preparedStatement.setString(1, user.getUsername());
                preparedStatement.setString(2, Util.getInstance().encrypt(user.getPassword()));
                preparedStatement.setString(3, user.getFirstname());
                preparedStatement.setString(4, user.getMiddlename());
                preparedStatement.setString(5, user.getLastname());
                preparedStatement.setDate(6, user.getDob());
                preparedStatement.setString(7, user.getEmail());
	            preparedStatement.addBatch();
            }
           	preparedStatement.executeBatch();
           	//connection.commit();
           	
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("**** \n User:"+user.getUsername()+"\t Dob:"+user.getDob()+"\t Email:"+user.getEmail()+"\n **********");
        }
    }

    /*public void deleteUsers() {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("delete from users");
            connection.setAutoCommit(false);
            preparedStatement.executeUpdate();
            connection.commit();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }*/

    public void updateUser(User user) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("update user set password=?, firstname=?, middlename=?, lastname=?, dob=?, email=? where username=?");
            preparedStatement.setString(1, Util.getInstance().encrypt(user.getPassword()));
            preparedStatement.setString(2, user.getFirstname());
            preparedStatement.setString(3, user.getMiddlename());
            preparedStatement.setString(4, user.getLastname());
            preparedStatement.setDate(5, user.getDob());
            preparedStatement.setString(6, user.getEmail());
            preparedStatement.setString(7, user.getUsername());
            preparedStatement.executeUpdate();
            //connection.commit();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteUser(String username) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("delete from user where username=?");
            preparedStatement.setString(1, username);
            preparedStatement.executeUpdate();
            //connection.commit();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public List<User> getAllUsers() {
        List<User> users = new ArrayList<User>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select * from user");
            while (rs.next()) {
                User user = new User();
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
                user.setFirstname(rs.getString("firstname"));
                user.setMiddlename(rs.getString("middlename"));
                user.setLastname(rs.getString("lastname"));
                user.setDob(rs.getDate("dob"));
                user.setEmail(rs.getString("email"));
                //System.out.println("--- Username:"+user.getUsername());
                users.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return users;
    }

    public User getUser(String username) {
        User user = null;
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select * from user where username='"+username+"'");
            while (rs.next()) {
                user = new User();
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
                user.setFirstname(rs.getString("firstname"));
                user.setMiddlename(rs.getString("middlename"));
                user.setLastname(rs.getString("lastname"));
                user.setDob(rs.getDate("dob"));
                user.setEmail(rs.getString("email"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

}