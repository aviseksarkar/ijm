package com.ijm.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.ijm.excel.ReadExcel;
import com.ijm.model.Case;
import com.ijm.util.Util;

public class NameListDao {

    private Connection connection;

    public NameListDao() {
        connection = Util.getInstance().getConnection();
    }

    public void addName(String name) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("insert into name_list(name) values (?)");
            //preparedStatement.setString(1, cas.getDate());
            preparedStatement.setString(1, name);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void addNames() {
        try {
            PreparedStatement preparedStatement = connection
            		.prepareStatement("insert into name_list(name) values (?)");

            //connection.setAutoCommit(false);
            
            ReadExcel readExcel = new ReadExcel(Util.getInstance().getCaseNameListFile());
            List<String> listNames = readExcel.read(1); 
            
            //the following if-else block added on 21-Apr-2016 to skip deleting the names if the excel file is not present
            // this is to incorporate the "http://[IP]:8080/ijm/partynames/" functionality 
            if (listNames.size() > 0) {
            	deleteAllNames();
            } else {
            	return;
            }
            
            for ( String name : listNames) {
            	if (name.trim().length() != 0 ) {
            		preparedStatement.setString(1, name);
            		preparedStatement.addBatch();
            	}
            }
           	preparedStatement.executeBatch();
           	//connection.commit();
           	
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addNames2() {
        try {
            PreparedStatement preparedStatement = connection
            		.prepareStatement("INSERT INTO name_list2(SELECT REPLACE(name,' ','') from name_list)");

            //connection.setAutoCommit(false);
            preparedStatement.executeUpdate();
            //connection.commit();
           	
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Deletes all entries from Name_List and Name_List2 tables.
     */
    public void deleteAllNames() {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("delete from name_list");
            preparedStatement.executeUpdate();
            preparedStatement = connection.prepareStatement("delete from name_list2");
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void deleteName(String name) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("delete from name_list where name='"+name+"'");
            // Parameters start with 1
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void updateName(String nameOriginal, String nameModified) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("update name_list set name='"+ nameModified +"' where name='"+nameOriginal+"'");
            // Parameters start with 1
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /*public void updateCase(Case cas) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("update users set firstname=?, lastname=?, dob=?, email=?" +
                            "where userid=?");
            // Parameters start with 1
            preparedStatement.setString(1, cas.getFirstName());
            preparedStatement.setString(2, cas.getLastName());
            preparedStatement.setDate(3, new java.sql.Date(cas.getDob().getTime()));
            preparedStatement.setString(4, cas.getEmail());
            preparedStatement.setInt(5, cas.getUserid());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }*/

    public List<String> getAllNames() {
        List<String> names = new ArrayList<String>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select name from name_list");
            while (rs.next()) {
                names.add(rs.getString("name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return names;
    }

}