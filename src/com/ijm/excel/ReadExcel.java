package com.ijm.excel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import com.ijm.util.Util;

public class ReadExcel {

	private String inputFile;
	
	public ReadExcel(String file) {
		inputFile = file;
	}
	
	public void setInputFile(String inputFile) {
		this.inputFile = inputFile;
	}

	/**
	 * 
	 * @param columns no. of Columns. If columns=0 then all columns will be read. Otherwise no. of columns will be read.
	 * @return
	 */
	public List<String> read(int columns) {
		List<String> list = new ArrayList<String>();
		File inputWorkbook = new File(inputFile);
		Workbook w;
		try {
			w = Workbook.getWorkbook(inputWorkbook);
			// Get the first sheet
			Sheet sheet = w.getSheet(0);
			
			int totalColumns = columns;
			if (columns == 0) {
				totalColumns = sheet.getColumns();
			}
			
			for (int j = 0; j < totalColumns; j++) {
				for (int i = 0; i < sheet.getRows(); i++) {
					if (i==0)//skip the header
						continue;
					Cell cell = sheet.getCell(j, i);
					String cell_content = cell.getContents();
					list.add(cell_content);
					//System.out.println(cell_content);
					/*CellType type = cell.getType();
					if (type == CellType.LABEL) {
						System.out.println("I got a label "
								+ cell.getContents());
					}

					if (type == CellType.NUMBER) {
						System.out.println("I got a number "
								+ cell.getContents());
					}*/
				}
			}
		} catch (BiffException e) {
			System.out.println("Error reading excel: "+inputFile);
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Error reading excel: "+inputFile);
			e.printStackTrace();
		}
		
		return list;
	}

	/*public static void main(String[] args) throws IOException {
		ReadExcel test = new ReadExcel(Util.getInstance().getCaseNameListFile());
		//test.setInputFile("/home/avisek/jxlrwtest.xls");
		//test.read();
		List<String> listNames = test.read(1); 
        for ( String name : listNames) {
            if (name.trim().length() != 0 )
            	System.out.println("- "+name);
        }
	}*/

}