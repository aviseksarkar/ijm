package com.ijm.domain;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ijm.dao.CasesDao;
import com.ijm.dao.EmailDao;
import com.ijm.dao.NameListDao;
import com.ijm.email.SendEmail;
import com.ijm.model.Case;
import com.ijm.model.CourtInfo;
import com.ijm.util.Util;

public class URLParser implements Job {
	
	private static URLParser instance = null;
	
	private static String STR_COURT_NO = "COURT NO.";
	private static String STR_JUSTICE = "HON'BLE JUSTICE";
	private static String STR_CHIEF_JUSTICE = "HON'BLE CHIEF JUSTICE";
	private static String STR_LEARNED_REGISTRAR = "LEARNED REGISTRAR"; //Example data as on 17-06-2015
	private static String STR_COURT_DELIMITER_BEGIN = "---------------------------------------------------------------------------";
	private static String STR_COURT_DELIMITER_END = "********************************************************************************";
	private static String STR_SUB_TYPE_DELIMITER_BEGIN = STR_COURT_DELIMITER_END; 
	private static String STR_SUB_TYPE_DELIMITER_END = "------";
													    
	private static String STR_TIME = "AT ";
	
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
	public static URLParser getInstance() {
		if ( instance == null) {
			instance = new URLParser();
			instance.parse();
		}
		return instance;
	}

	public URLParser() {
		//System.out.println("URL Parser ");
	}
	
	public void execute(JobExecutionContext context) throws JobExecutionException {
		//System.out.println("..... IJM Job execute.....");
		parse();
	}
	
	public static void main(String z[]) {
		List<CourtInfo> courts = null;
		
		/*if(true) {
			new URLParser().sendEmail(new CasesDao().getAllFilteredCases(), null);
			//SendEmail.getInstance().sendEmail(null, null);
			//Util.getInstance().downloadPDF();
			return;
		}*/
		
		if (z.length != 0) {
			BufferedReader reader = null;
			StringBuilder sb = new StringBuilder();
			try {
				reader = new BufferedReader(new InputStreamReader(new FileInputStream(Util.getInstance().getArchiveFolder()+"/"+z[0])));
				String inputLine;
				while ((inputLine = reader.readLine()) != null) {
					sb.append(inputLine+"\n");
					//System.out.println("-" + inputLine);
				}
				courts = URLParser.getInstance().parse(sb.toString()); 
			} catch (IOException e) {
				System.out.println("Problems during file upload. Error:" + e);
			} finally {
				if (reader != null) {
					try {
						reader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			URLParser.getInstance();
			/*courts = URLParser.getInstance().parse(null); //the getinstance() calls the parse()
			if (courts != null) {
				for (CourtInfo court : courts) {
					System.out.println("------------------- Court No.:"+court.getCourtNumber()+"---------------------");
					for (Case cas : court.getCases()) {
						System.out.println("Date:"+cas.getDate()+"\tJustice:"+cas.getJusticeName()+"\tParty:"+cas.getPartyName());
					}
				}
			}*/		
		}
		//System.out.println("main() Size : "+courts.size());
	}
	
	public void parse(){
		parse(null);
	}
	
    public List<CourtInfo> parse(String contentArchive) {
    	boolean isMonthly = false;
    	
    	List<CourtInfo> courts = new ArrayList<CourtInfo>();
    	List<Case> casesAll = new ArrayList<Case>();
    	EmailDao emailDao = new EmailDao();
		NameListDao namesDao = new NameListDao();
		CasesDao casesDao = new CasesDao();

        //String urlString = Util.getInstance().getCaseUrl();
        java.sql.Date sqlDate = null;
        boolean dateCheck = true; //check the latest date from DB and parse if required
        StringBuilder sbArchiveBuilder = new StringBuilder(10000);
        
        //for (String urlString : urlStrings) {
        	/*URL url = null;
	        try {
				url = new URL(urlString);
				//System.out.println("*************************   URL: "+urlString+"\n");
			} catch (MalformedURLException e) {
				System.out.println("Error - Malformed URL : "+urlString);
				e.printStackTrace();
				return null;
			}*/
	        BufferedReader contentReader = null;
	        //Reader reader = null;
			try {
				/*if (contentArchive != null) {//for archive content validation
					reader = new StringReader(contentArchive);
					//dateCheck = false;
				} else {
					reader = new InputStreamReader(url.openStream());//for url content parsing
				}*/
				
					//contentReader = new BufferedReader(reader);
					//String content = Util.getInstance().pdfToText("C:/avisek/personal/workspace/projects/IJM/test/00320012016.pdf");
					//TODO uncomment the above line
					//contentReader = new BufferedReader(new StringReader(content));
					
					if ( Util.getInstance().getDownloadPdfStatus() )
						isMonthly = Util.getInstance().downloadPDF();
					contentReader = new BufferedReader(new FileReader(Util.getInstance().getHomeFolder()+Util.getInstance().getTextFileName())); //For AWS
					//contentReader = new BufferedReader(new InputStreamReader(new FileInputStream(Util.getInstance().getHomeFolder()+Util.getInstance().getTextFileName()), Charset.forName("UTF-8"))); //For AWS
					//contentReader = new BufferedReader(new FileReader("PDF_Content.txt"));
					//contentReader = new BufferedReader(new FileReader("PDF_Content_test.txt"));
					
					String inputLine;
					List<List<String>> missedCases = new ArrayList<List<String>>();

					String strDate = "";
					String strCaseSerialNo;
				    String strCaseNumber;
				    String strPartyName = "";
				    String strCaseAgainst = "";
				    String strAttorneyName = "";
				    String strCourtNumber;
				    String strTime;
				    String strJusticeName;
				    String strNotes;
				    
				    String strPrevious = "";
				    
				    String strTemp[] = new String[2];
					
				    List<Case> cases = new ArrayList<Case>();
				    List<Case> courtCases = new ArrayList<Case>();
					while ((inputLine = contentReader.readLine()) != null) {
						sbArchiveBuilder.append(inputLine+"\n");
						inputLine = inputLine.trim();
						if ( inputLine.startsWith(STR_COURT_NO) ) {
							//Re-Initialize the variables - once for every COURT NO.
							strJusticeName = "";
							strTime = "";
							strCourtNumber = "";
							strDate = "";
							
							strDate = strPrevious;
							//System.out.println("Date:"+strDate);
							strCourtNumber = inputLine.substring(STR_COURT_NO.length()).trim();
							//As on 10-June-2016 JUSTICE ASHIS KUMAR CHAKRABORTY did not have a court no. causing blank data inserted into Court_info table
							//and getFileteredCases() resulted NullPointerException
							if (strCourtNumber.trim().length() == 0) {
								strCourtNumber = "0";
							}
							//System.out.println("court:"+strCourtNumber);
							
							if(dateCheck) {
								//set the date which is in dd/MM/yyyy format
								if (strDate.length() == 10){
									String year = strDate.substring(6);
									String month = strDate.substring(3,5);
									String day = strDate.substring(0,2);
									String strSQLDt = year+"-"+month+"-"+(day);
									
									sqlDate = java.sql.Date.valueOf(strSQLDt);
									//System.out.println("URL Date = "+strSQLDt+"\t\t"+sqlDate);

									if (contentArchive == null) {
										Date dtMax = casesDao.getMaxDate();
										//System.out.println("\n Existing CaseDate Max = "+dtMax+"\n");
										//there will be no date for the first time application is run
										if (dtMax != null ) {
											//Do not carry on with the parsing and saving of data if latest date is already present
											if( ! (dtMax.before(sqlDate)) ) {
												System.out.println(" \n ------------ "+strDate+" data already processed ------------ \n");
												return null;
											} else {
												//TODO
												System.out.println(new java.util.Date(System.currentTimeMillis())+"\tProcessing the data of "+strDate);
											}
										}
									}
									dateCheck = false;
								}
							}
							
							StringBuilder sbNotes = new StringBuilder();
							
							//COURT NO. loop
							while ((inputLine = contentReader.readLine()) != null) {
								try {
									sbArchiveBuilder.append(inputLine+"\n");
									String sourceLine = new String(inputLine);//for temp storage as inputLine is trimmed in subsequent steps
									inputLine = inputLine.trim();
									if (inputLine.equals(STR_COURT_DELIMITER_END)){
										//12-Apr-2016 ... replaced equals with contains as the no. of dashes were less in Court No. 26
										if(strPrevious.contains(STR_COURT_DELIMITER_BEGIN)){
											break;
										}
									}
									//SUBTYPE / NOTES
									if (inputLine.equals(STR_SUB_TYPE_DELIMITER_BEGIN) && (strPrevious.startsWith(STR_JUSTICE) || strPrevious.startsWith(STR_CHIEF_JUSTICE) || strPrevious.startsWith(STR_LEARNED_REGISTRAR))) {
										boolean isVsPresent = false;
										while ((inputLine = contentReader.readLine()) != null) {
											sbArchiveBuilder.append(inputLine+"\n");
											sourceLine = new String(inputLine); 
											if (inputLine.trim().contains(STR_COURT_DELIMITER_BEGIN)){//for situations where there is no data for court no. after subtype
												//System.out.println("No Subtype/Notes data for CourtNumber:"+strCourtNumber);
												strPrevious = inputLine.trim();
												break;
											} else if ( (inputLine.trim()).contains(STR_SUB_TYPE_DELIMITER_END) && inputLine.substring(5).contains(STR_SUB_TYPE_DELIMITER_END) ){
												break;
											} else if (inputLine.contains("Vs") && (! inputLine.contains("Vs.")) && (! inputLine.contains("-Vs-"))) {
												String tmp_strCaseSerialNo = sourceLine.substring(0,6).trim();
												if (tmp_strCaseSerialNo.endsWith(".")) {
													isVsPresent = true;
													break;
												}
											}
											sbNotes.append(inputLine.trim());
											//System.out.println(inputLine);
										}
										//System.out.println("$$$$  Notes Length: "+sbNotes.length()+" \t\t Court: "+strCourtNumber);
										if (! isVsPresent)
										{
											if (inputLine.length() > 0) {
												strPrevious = inputLine;
											}
											continue;
										}
									}
									/*if (inputLine.length() > 0) {
										strPrevious = inputLine;
									}*/
									//System.out.println("------"+inputLine);
									if (inputLine.startsWith(STR_JUSTICE)){
										strJusticeName += inputLine.substring(STR_JUSTICE.length()).trim()+"; ";
										//System.out.println(strJusticeName);
										if (inputLine.length() > 0) {
											strPrevious = inputLine;
										}
										continue;
									} else if (inputLine.startsWith(STR_CHIEF_JUSTICE)){
										strJusticeName = inputLine.substring(STR_CHIEF_JUSTICE.length()).trim()+"; ";
										//System.out.println(strJusticeName);
										if (inputLine.length() > 0) {
											strPrevious = inputLine;
										}
										continue;
									} else if (inputLine.startsWith(STR_LEARNED_REGISTRAR)){
										strJusticeName = inputLine.trim()+"; ";
										//System.out.println(strJusticeName);
										if (inputLine.length() > 0) {
											strPrevious = inputLine;
										}
										continue;
									} else if (inputLine.startsWith(STR_TIME)){
										strTime = inputLine.substring(STR_TIME.length()).trim();
										if (strTime.length() > 15) //for occurence  where the Time is part of a long string for a different day
											strTime = "";
										//System.out.println("time:"+strTime);
										if (inputLine.length() > 0) {
											strPrevious = inputLine;
										}
										continue;
									} else if (inputLine.contains("Vs") && ( (! inputLine.contains("Vs.")) || (! inputLine.contains("-Vs-")) ) ){
										try{
											boolean executeStrPreviousLoop = true;
											//for cases wherein the party name is long enough and Vs appears in the second line
											//then read the previous line and check if case_serial_no. is present
											try{
												if (Integer.parseInt(strPrevious.substring(0, 1)) > 0) {//as all cases start with a number
													try {
														if (Integer.parseInt(inputLine.substring(0, 1)) > 0)
															executeStrPreviousLoop = false;
														//Do nothing skip the if(strPrevious...) block
														//InputLine contains valid case and the strPrevious contains Bad Case which needs to be skipped.. for example
														//12.  SMA 131/2010
														//
														//13.  SAT 1993/2007   DULAL SARDAR Vs                                   
														//SAIBALENDU BHOWMIK
														//                     BASANBALA MONDAL & ORS
													}catch (NumberFormatException ne) {
														executeStrPreviousLoop = true;
													}
													
													if(executeStrPreviousLoop){
														//System.out.println("$$$\n inputLine="+inputLine+"\n"+"strPrevious="+strPrevious);
														inputLine = strPrevious+inputLine;
														//In this case after the Vs the Attorney Name appears... for example...
														//45.  CRM 223/2016    AVADHUTIKA ANANDA NIRMOHA ACHARYA @ A.NIRMALA ACHARIYA & 
														//ORS VsAPALAK BASU
														//					   STATE OF WEST BENGAL
														strTemp = inputLine.substring(21).split("Vs");
														strPartyName = strTemp[0].trim();
														if (strTemp.length > 1)
															strAttorneyName = strTemp[1].trim();
													}
												}
											}catch(NumberFormatException e) {
												//System.out.println("NFE : "+inputLine);
												//inputLine = strPrevious.concat(new String(inputLine));
											}
										}catch(NumberFormatException e) {
											//System.out.println("NFE : "+inputLine);
										}
										try{
											if (! (Integer.parseInt(inputLine.substring(0, 1)) > 0) )	//as all cases start with a number
											{
												if (inputLine.length() > 0) {
													strPrevious = inputLine;
												}
												continue;
											}
										}catch(NumberFormatException e) {
											//System.out.println("NFE : "+inputLine);
											if (inputLine.length() > 0) {
												strPrevious = inputLine;
											}
											continue;
										}
										strCaseSerialNo = inputLine.substring(0,6).trim();
										//strCaseSerialNo = sourceLine.substring(0,6).trim();
										if (strCaseSerialNo.length() == 0) {
											if (inputLine.length() > 0) {
												strPrevious = inputLine;
											}
											continue;
										}
										//some times the a number is present but does not end with '.' For example 
										//6) CRIMINAL MOTIONS WILL BE TAKEN UP FROM CRR 1558/2015 ( SHYAMAL ROY AND ANR. Vs 
										//The above line should be skipped
										/*if (!strCaseSerialNo.endsWith(".")) {
											continue;
										}*/
										try {
											strCaseSerialNo = strCaseSerialNo.substring(0, strCaseSerialNo.lastIndexOf('.'));
										} catch (StringIndexOutOfBoundsException e) {
											System.out.println("\n- SourceLine="+sourceLine+"\n\n Exiting the processing of data due to unsupported format...\n");
											e.printStackTrace();
											if (inputLine.length() > 0) {
												strPrevious = inputLine;
											}
											continue;
										}
										//**** CaseNumber starts @ offset 5
										//**** PartyName starts @ offset 21
										//**** AttorneyName starts @ offset 71 and may go to next line
										//**** CaseAgainst starts @ offset 21 mostly in 2nd or 3rd Line
										
										strCaseNumber = inputLine.substring(5, 21);
										
										if ( strPartyName.equals("")) { //PartyName is processed if Vs is present in SecondLine
											strTemp = inputLine.substring(21).split("Vs");
											//strPartyName = strTemp[0].substring(21, 71).trim(); //TODO : modified to pick PartyName
											strPartyName = strTemp[0].trim(); //TODO : modified to pick PartyName
											/*if ( strTemp.length > 1 ) { //for cases where Attorney Name is not present
											strAttorneyName = strTemp[1].trim();
										}*///commented as it is no more required with the new PDF Format
										}
										
										if (strAttorneyName.equals("") && inputLine.length() > 71)//PartyName is processed if Vs is present in SecondLine
											strAttorneyName = inputLine.substring(71);
										
										if (inputLine.length() > 0) {
											strPrevious = inputLine;
										}
										
										//System.out.println("---- "+inputLine);
										//for AttorneyName if it is present in the 2nd line.
										if ((inputLine = contentReader.readLine()) != null ) {
											if (inputLine.trim().length() == 0) {
												continue;
											}
											
											if (! Character.toString(inputLine.charAt(0)).equals(" ")) {
												strAttorneyName += inputLine.trim();
											} else {// CaseAgainst is present in 2nd line
												strCaseAgainst = inputLine.substring(21).trim();
											}
										}
										
										//System.out.println("**** "+inputLine);
										//for CaseAgainst if it is present in the 3rd line.
										if ((inputLine = contentReader.readLine()) != null ) {
											if (inputLine.trim().length() == 0 && strCaseAgainst.equals("") ) {
												continue;
											}
											
											//System.out.println("== "+inputLine);
											if (inputLine.length() > 21) {
												if (!Character.toString(inputLine.charAt(21)).equals(" ")) {
													strCaseAgainst = inputLine.substring(21).trim();
												}
											}
										}
										
										/*if ((inputLine = contentReader.readLine()) != null && inputLine.length() > 21) { //sometimes CaseAgainst is not present
											sbArchiveBuilder.append(inputLine+"\n");
											if (inputLine.trim().equals(STR_COURT_DELIMITER_BEGIN)) //for case which is the last in the court and does not have CaseAgainst or have the CaseAgainst in the same line of 'Vs'
												strPrevious = inputLine;
											else
												strCaseAgainst = inputLine.substring(21).trim();
										}*///commented as it is no more required with the new PDF Format
										
										
										
										//Build the Case details
										Case cas = new Case();
										cas.setCourtNumber(strCourtNumber);
										cas.setTime(strTime);
										cas.setCaseSerialNumber(strCaseSerialNo);
										cas.setCaseNumber(strCaseNumber);
										cas.setPartyName(strPartyName);
										cas.setCaseAgainst(strCaseAgainst);
										cas.setAttorneyName(strAttorneyName);
										//System.out.println("-- strJusticeName -"+strJusticeName);
										//try{
											if (strJusticeName.trim().length() != 0 && strJusticeName.trim().contains(";"))
												cas.setJusticeName(strJusticeName.trim().substring(0, strJusticeName.lastIndexOf(';')));
										/*} catch (StringIndexOutOfBoundsException e) {
											System.out.println("***   "+strJusticeName);
											e.printStackTrace();
										}*/
										//add to the Case List
									    cases.add(cas);
									    courtCases.add(cas);
									    //System.out.println("CaseNumber:"+strCaseNumber+"\t PartyName:"+strPartyName+"\t CaseAgainst:"+strCaseAgainst+"\t AttorneyName:"+strAttorneyName);
									    
										//Re-Initialize the variables
									    strCaseNumber = "";
									    strPartyName = "";
									    strCaseAgainst = "";
									    strAttorneyName = "";
									    
									} //Party,CaseAgainst condition
									
									if (inputLine != null && inputLine.length() > 0) {
										strPrevious = inputLine;
									}
									//System.out.println("Court No. ------ ******* ");
								} catch (Exception e) {
									List<String> list = new ArrayList<String>();
									list.add(strPrevious);
									list.add(inputLine);
									missedCases.add(list);

									System.out.println(strPrevious);
									System.out.println(inputLine);
									
									e.printStackTrace();
								}
							}//end of COURT NO. loop
							
							CourtInfo court = new CourtInfo();
							court.setCourtNumber(strCourtNumber);
							if (strJusticeName.trim().length() != 0 && strJusticeName.trim().contains(";"))
								court.setJusticeName(strJusticeName.trim().substring(0, strJusticeName.lastIndexOf(';')));
							court.setNotes(sbNotes.toString());
							court.setCases(courtCases);
							courts.add(court);
							courtCases = new ArrayList<Case>();
						} else {
							if (inputLine.length() > 0) {
								strPrevious = inputLine;
							}
							continue;
						}
						
						//System.out.println("*previous : "+strPrevious);
						//System.out.println("*source : "+inputLine);
					}
					
					/*if (true)//TODO delete this section
						return null;*/

					//Iterate and set the date on all cases & courts
					for(Case ca : cases){
						ca.setDate(sqlDate);
					}
					for(CourtInfo court : courts){
						court.setDate(sqlDate);
					}
					
					if (contentArchive != null) { //for archive file validation
						casesAll.addAll(cases);
						//System.out.println("casesAll size : "+casesAll.size());
						//return;
					} else {
						//namesDao.deleteAllNames();	//commented on 21-Apr-2016. Included in addNames() function
						emailDao.addEmails();
						namesDao.addNames();
						namesDao.addNames2();
						casesDao.deleteCases();
						casesDao.deleteTempCases();
						casesDao.deleteAliasCases();
						casesDao.addCases(cases);
						casesDao.addCourts(courts);
						casesDao.populateTempCases();
						casesDao.populateAliasCases();
						casesDao.populateFilteredCases();
						
						System.out.println("\n"+dateFormat.format(new Date())+"- DB tables populated successfully !!!\n");
						if ( Util.getInstance().getSendEmailStatus()) {
							sendEmail(casesDao.getAllFilteredCases(), missedCases, sqlDate.toString(), isMonthly);
						}
						//Write the archive file
						//writeArchiveFile(sbArchiveBuilder, sqlDate);
					}
					//reader.close();
					contentReader.close();
			} catch (IOException e) {
				System.out.println("Error reading the content from URL : ");
				e.printStackTrace();
				return null;
			} /*catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} */finally {
				try {
					if (contentReader != null)
						contentReader.close();
					//if (reader != null) reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	    //}//for loop urlStrings
        
        return courts;
	}
    
    /*private void writeArchiveFile(StringBuilder sbArchiveBuilder, Date sqlDate) {
    	if (sbArchiveBuilder.length() > 0) {
			String archiveFile = Util.getInstance().getArchiveFolder()+""+Util.getInstance().getArchiveFileTemplate()+""+sqlDate.toString();
			//System.out.println("ArchiveFile : "+archiveFile);
			PrintWriter archiveFileWriter = null;
			BufferedReader archiveBuilderReader = null;
			try {
				archiveFileWriter = new PrintWriter( new FileOutputStream(archiveFile), true);
				archiveBuilderReader = new BufferedReader(new StringReader(sbArchiveBuilder.toString()));
				String line;
				while ((line = archiveBuilderReader.readLine()) != null) {
					archiveFileWriter.println(line);
				}
			} catch (FileNotFoundException e) {
				System.out.println("Error creating Archive File "+ archiveFile);
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println("Error reading archive reader ...");
				e.printStackTrace();
			} finally {
				if (archiveFileWriter != null)
					archiveFileWriter.close();
				try {
					archiveBuilderReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
    }*/
    
    public void sendEmail(List<CourtInfo> courts, List<List<String>> listsMissedCases, String date, boolean isMonthly){
    	//String date = null;
    	StringBuilder sb = new StringBuilder();
    	sb.append("<HTML><BODY>");
    	sb.append("<table style=\"width:100%\" border=\"1\" background-color: #f1f1c1;>");
    	sb.append("<thead>");
    	sb.append("<tr>");
    	sb.append("<th>Court No</th>");
    	sb.append("<th>Date</th>");
    	sb.append("<th>Case Serial</th>");
    	sb.append("<th>Case No</th>");
    	sb.append("<th>Party</th>");
    	sb.append("<th>Party Against</th>");
    	sb.append("<th>Attorney</th>");
    	sb.append("<th>Justice</th>");
    	//sb.append("<th>Time</th>");
    	sb.append("</tr>");
    	sb.append("</thead>");
    	sb.append("<tbody>");

    	for(CourtInfo court : courts) {
    		boolean rowcheck=true;
     		for(Case cas : court.getCases()) {
     			//date = cas.getDate().toString();
     			sb.append("<tr>");
     			if(rowcheck)
     				sb.append("<td rowspan=\""+court.getCases().size()+"\" title=\""+court.getNotes()+"\" >"+cas.getCourtNumber()+"</td>");
     			sb.append("<td>"+cas.getDate()+"</td>");
    	    	sb.append("<td>"+cas.getCaseSerialNumber()+"</td>");
    	    	sb.append("<td>"+cas.getCaseNumber()+"</td>");
    	    	sb.append("<td>"+cas.getPartyName()+"</td>");
    	    	sb.append("<td>"+cas.getCaseAgainst()+"</td>");
    	    	sb.append("<td>"+cas.getAttorneyName()+"</td>");
    	    	if(rowcheck) {
    	    		sb.append("<td rowspan=\""+court.getCases().size()+"\" >"+cas.getJusticeName()+"</td>");
    	    		rowcheck = false;
    	    	}
    	    	//sb.append("<td>"+cas.getTime()+"</td>");
     			sb.append("</tr>");
     		}
    	}
    	sb.append("</tbody>");
    	sb.append("</table>"); 
    	
    	if (! listsMissedCases.isEmpty())
    		sb.append("<h2 style=\"color:red\"><u>Cases having bad format</u></h2>");
		for (List<String> list : listsMissedCases) {
			//sb.append("<BR>");
			sb.append(list.get(0));
			sb.append("<BR>");
			sb.append(list.get(1));
			sb.append("<BR>");
			sb.append("<BR>");
		}
    	
    	sb.append("</BODY></HTML>");
    	
    	//System.out.println(sb.toString());
    	
    	if (isMonthly)
    		new SendEmail().sendEmail(sb.toString(), "Monthly - "+date);
    	else 
    		new SendEmail().sendEmail(sb.toString(), date);
    }
}