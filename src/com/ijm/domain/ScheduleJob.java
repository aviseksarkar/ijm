package com.ijm.domain;

import java.text.ParseException;
import java.util.Date;

import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.triggers.CronTriggerImpl;

import com.ijm.util.Util;

public class ScheduleJob {

	private static ScheduleJob instance = null;
	Scheduler schedule = null;
	JobKey jobKey = JobKey.jobKey("IJMScheduler");

	private ScheduleJob() {
		try {
			SchedulerFactory sf = new StdSchedulerFactory();
			Scheduler schedule = sf.getScheduler();
			JobDetailImpl job = new JobDetailImpl();
			job.setKey(jobKey);
			job.setJobClass(URLParser.class);

			CronTriggerImpl trigger = new CronTriggerImpl();
			trigger.setName("IJMTrigger");
			try {
				trigger.setCronExpression("0 0 "+Util.getInstance().getSchedule()+" * * ?");
			} catch (ParseException e) {
				e.printStackTrace();
			}
			// trigg
			// .withSchedule(cronSchedule("0,30 * * ? * MON-FRI"))
			// .build();

			Date ft = schedule.scheduleJob(job, trigger);
			System.out.println(job.getKey() + " has been scheduled to run at: "
					+ ft + " and repeat based on expression: "
					+ trigger.getCronExpression());
			schedule.start();
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}
	
	public void stop() {
		try {
			System.out.println("IJMScheduler shuting down....");
			schedule.pauseJob(jobKey);
			schedule.shutdown(true);
			schedule.clear();
		} catch (SchedulerException e) {
			System.out.println("IJMScheduler shuting down issue .... JobKey"+jobKey);
			e.printStackTrace();
		}
		schedule = null;
	}

	public static ScheduleJob getInstance() {
		if (instance == null) {
			instance = new ScheduleJob();
		}
		return instance;
	}
}
