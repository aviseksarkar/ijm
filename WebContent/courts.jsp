<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@include file="header.html" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">

  
      <meta charset="utf-8">
      <title>IJM Search Results</title>
	  <!-- styles -->
		<link rel="stylesheet" type="text/css" href="css/layerslider.css">
		<link rel="stylesheet" type="text/css" href="css/skin.css">
		<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="css/owl.css">
		<link rel="stylesheet" type="text/css" href="css/jquery.css">
		<link rel="stylesheet" type="text/css" href="css/styles.css">
		<link rel="stylesheet" type="text/css" href="css/color-blue.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link href="css/style_admin.css" rel="stylesheet" type="text/css" />
		 <!-- styles -->
   <style>
table#search_results {
	border : 2px solid black;
	border-spacing: 5px;
    width: 100%;
    background-color: white;
}
table#search_results th {
	border : 2px solid black;
	font: 18px bold;
    color: white;
    background-color: blue;
} 

table#search_results tr {
	border : 2px solid black;
    text-align: center;
} 
table#search_results td {
	border : 2px solid black;
    text-align: center;
}
</style>	 
</head>

<body background="images/bg.png">
<!-- logo -->

<a href="javascript:history.back()"> &nbsp;&nbsp;&nbsp; <u>Back</u> </a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <B>Date : <c:out value="${courtDate}" /></B>

<br></br>

 
<div id="tableData">
<table id="search_results" style="width:100%" border="1" background-color: #f1f1c1;>
 <thead>
  <tr>
    <th>Court No.</th>
    <th>Notes</th>
    <th>Justice</th>
  </tr>
 </thead>
 <tbody>
  <c:forEach items="${courts}" var="court">
   <tr>
    <td><c:out value="${court.courtNumber}" /></td>
    <td><c:out value="${court.notes}" /></td>
    <td><c:out value="${court.justiceName}" /></td>
  </tr>
 </c:forEach>
</tbody>
</table>  
</div>

</body></html>
