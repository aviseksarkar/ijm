<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@include file="header.html" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">

  
      <meta charset="utf-8">
      <title>IJM Search Results</title>
	  <!-- styles -->
		<link rel="stylesheet" type="text/css" href="css/layerslider.css">
		<link rel="stylesheet" type="text/css" href="css/skin.css">
		<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="css/owl.css">
		<link rel="stylesheet" type="text/css" href="css/jquery.css">
		<link rel="stylesheet" type="text/css" href="css/styles.css">
		<link rel="stylesheet" type="text/css" href="css/color-blue.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link href="css/style_admin.css" rel="stylesheet" type="text/css" />
		 <!-- styles -->
   <style>
table#search_results {
	border : 2px solid black;
	border-spacing: 5px;
    width: 100%;
    background-color: white;
}
table#search_results th {
	border : 2px solid black;
	font: 18px bold;
    color: white;
    background-color: blue;
} 

table#search_results tr {
	border : 2px solid black;
    text-align: center;
} 
table#search_results td {
	border : 2px solid black;
    text-align: center;
}
</style>	 
</head>

<body background="images/bg.png">
<!-- logo -->


<br></br>
<!-- <table id="admin_controls" style="width:100%" border="0" >
<thead>
<tr>
<th> -->
<form name="form_validate_archive" id="form_validate_archive" action="." method="post" enctype="multipart/form-data">
    File:
    <input type="file" name="file">
    <input type="submit" value="Validate">
    <input type="hidden" name="command" value="Validate" >
</form>
<!-- </th>

<th> -->
<form method="post" action="." name="displayuser_form" align="layerslider">
<input type="submit" value="Create User" align="layerslider" class="custom-btn wpb_button wpb_btn-rounded wpb_btn-small" >
<input type="hidden" name="command" value="displayuserform" >
</form>
<!-- </th>
</tr>
</thead>
</table> -->
<br></br>

<form method="post" action="." name="displaynamelist_form" align="layerslider">
<input type="submit" value="Display Names" align="layerslider" class="custom-btn wpb_button wpb_btn-rounded wpb_btn-small" >
<input type="hidden" name="command" value="displaynamelistform" >
</form>

 
<div id="tableData">
<table id="search_results" style="width:100%" border="1" background-color: #f1f1c1;>
 <thead>
  <tr>
    <th>Username</th>
    <th>First Name</th>
    <th>Last Name</th>
    <th>Email ID</th>
    <th>Edit</th>
	<th>Delete</th>
  </tr>
 </thead>
 <tbody>
  <c:forEach items="${users}" var="user">
   <tr>
    <td><c:out value="${user.username}" /></td>
    <td><c:out value="${user.firstname}" /></td>
    <td><c:out value="${user.lastname}" /></td>
    <td><c:out value="${user.email}" /></td>
	<td align='center'><form action="." name="formuseredit" method="post"><input type="submit" value="Edit"><input type="hidden" name="command" value="useredit"><input type="hidden" name="username" value="<c:out value="${user.username}" /> "></form></td>
	<td align='center'>
	<c:if test="${user.username != 'admin' }" >
	<form action="." name="formuserdelete" method="post">
	<input type="submit" value="Delete"><input type="hidden" name="command" value="userdelete">
	<input type="hidden" name="username" value="<c:out value="${user.username}" /> "> 
	</form>
	</c:if> 
	</td>
  </tr>
 </c:forEach>
</tbody>
</table>  
</div>

</body></html>
