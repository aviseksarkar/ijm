<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@include file="header.html" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head></head>
<body style="width: 100%; height: 100%;">

<div style="top: 25%; left:18%;position: absolute;">
<form method="post" action="." name="createuser_form">
<tr>
<td><font size="3"  color="white">* Username:</font></td>
<td><input type="text" name="username" style="font-size:10pt;height:10pt" value=<c:out value="${user.username}" />></td>
<td><font size="3" color="white">* Password:</font></td>
<td><input type="password" name="password" style="font-size:10pt;height:10pt"></td>
<td><font size="3" color="white">* First Name:</font></td>
<input type="text" name="firstname" style="font-size:10pt;height:10pt" value=<c:out value="${user.firstname}" />>
<td><font size="3" color="white">Middle Name:</font></td>
<td><input type="text" name="middlename" style="font-size:10pt;height:10pt" value=<c:out value="${user.middlename}" />></td>
<td><font size="3" color="white">* Last Name:</font></td>	
<td><input type="text" name="lastname" style="font-size:10pt;height:10pt" value=<c:out value="${user.lastname}" />></td>
<td><font size="3" color="white">DOB:</font></td>
<td><input type="date" name="dob" style="font-size:10pt;height:10pt;border:2;" value="<fmt:formatDate pattern="MM/dd/yyyy" value="${user.dob}" />" /> <!-- <font size="3" color="white">MM/dd/yyyy</font> --></td>
<td><font size="3" color="white">* E-mail id:</font></td>
<td><input type="text" name="email" style="font-size:10pt;height:10pt" value=<c:out value="${user.email}" />></td>
</tr>
</br>

<input type="submit" name="command" value="Create User" size="3" color="white" class="custom-btn wpb_button wpb_btn-rounded wpb_btn-small"> 
<input type="submit" name="command" value="Display Users" size="3" color="white" class="custom-btn wpb_button wpb_btn-rounded wpb_btn-small">
<input type="submit" name="command" value="List Cases" size="3" color="white" class="custom-btn wpb_button wpb_btn-rounded wpb_btn-small">
<br>
<c:out value="${page_error}" escapeXml="false"/>

</form>
</div>

<img src="images/createuser.png" width="1260" height="575" alt=""/> 
  </body>



</html>
