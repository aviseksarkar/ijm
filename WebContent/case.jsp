<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<link type="text/css"
    href="css/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>
<title>IJM - Add New Case</title>
</head>
<body>
    <script>
        $(function() {
            $('input[name=date]').datepicker();
        });
    </script>

    <form method="POST" action='Controller' name="frmAddCase">
        ID : <input type="text" readonly="readonly" name="userid"
            value="<c:out value="${cas.id}" />" /> <br /> 
        <!-- Case Date : <input type="text" name="date"
            value="<c:out value="${cas.date}" />" /> <br /> -->
        Case Date : <input type="text" name="date"
            value="<fmt:formatDate pattern="MM/dd/yyyy" value="${cas.date}" />" /> <br /> 
        Court Number : <input type="text" name="courtNumber"
            value="<c:out value="${cas.courtNumber}" />" /> <br /> 
        Case Time : <input type="text" name="time"
            value="<c:out value="${cas.time}" />" /> <br /> 
        Case Number : <input type="text" name="caseNumber"
            value="<c:out value="${cas.caseNumber}" />" /> <br />
        Party Name : <input type="text" name="partyName"
            value="<c:out value="${cas.partyName}" />" /> <br />
        Case Against : <input type="text" name="caseAgainst"
            value="<c:out value="${cas.caseAgainst}" />" /> <br />
        Attorney Name : <input type="text" name="attorneyName"
            value="<c:out value="${cas.attorneyName}" />" /> <br />     
            <input type="submit" value="Submit" />
    </form>
</body>
</html>