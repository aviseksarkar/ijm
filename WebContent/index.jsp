<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@include file="header.html" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">

  
      <meta charset="utf-8">
      <title>IJM Search Results</title>
	  <!-- styles -->
		<link rel="stylesheet" type="text/css" href="css/layerslider.css">
		<link rel="stylesheet" type="text/css" href="css/skin.css">
		<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="css/owl.css">
		<link rel="stylesheet" type="text/css" href="css/jquery.css">
		<link rel="stylesheet" type="text/css" href="css/styles.css">
		<link rel="stylesheet" type="text/css" href="css/color-blue.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">
		 <!-- styles -->
   <style>
table#search_results {
	border : 2px solid black;
	border-spacing: 5px;
    width: 100%;
    background-color: white;
}
table#search_results th {
	border : 2px solid black;
	font: 18px bold;
    color: white;
    background-color: blue;
} 

table#search_results tr {
	border : 2px solid black;
    text-align: center;
} 
table#search_results td {
	border : 2px solid black;
    text-align: center;
}
</style>	 
</head>

<body background="images/bg.png">
<!-- logo -->
<!-- <header class="page-header main-page">
					<section id="logo" class="logo">
					<div>
						<a href="index.jsp"><img src="images/ijmlogo.png" alt="IJM Logo"></a>
					</div>
				</section>
				<section id="logout" class="logo">
					<div>
						<form action="." name="logout_form">
                          	<input type="submit" name="command" value="logout" class="custom-btn wpb_button wpb_btn-rounded wpb_btn-small" >
						</form>
					</div>
				</section>
				
</header>
 -->

<!-- search Banner -->
 
			<div id="home_quick_search">
				<div id="filterForm" class="wpb_row vc_row-fluid">
					<div class="col-centered">
						<form action="." name="party">
							<fieldset>
								<h2 class="pull-left">Party </h2>
								<div class="input">
									<input type="text" name="party" value="">
								</div>
								
								<h2 class="pull-left">Attorney </h2>
								<div class="input">
                                <input type="text" name="attorney" value="">
									
								</div>
								
								<div class="pull-left">
									<input type="hidden" name="command" value="search" class="custom-btn wpb_button wpb_btn-rounded wpb_btn-small" >
                          			<input type="submit" value="SEARCH" class="custom-btn wpb_button wpb_btn-rounded wpb_btn-small" >	
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
<!--/ quick search -->
  
  <br></br>
<div id="tableData">
<table id="search_results" style="width:100%" border="1" background-color: #f1f1c1;>
 <thead>
  <tr>
    <th>Court No</th>
    <th>Date</th>
    <th>Case Serial</th>
    <th>Case No</th>
    <th>Party</th>
	<th>Party Against</th>
	<th>Attorney</th>
	<th>Justice</th>
	<!-- <th>Time</th> -->
  </tr>
 </thead>
 <tbody>
  <c:forEach items="${courts}" var="court">
  	<c:set var="rowcheck" value="true" scope="page" />
 	<c:forEach items="${court.cases}" var="cas">
	   <tr>
	    <c:if test="${rowcheck == 'true' }" >
		 <td rowspan="<c:out value="${court.cases.size()}" />" title="<c:out value="${court.notes}" />" ><a href="/ijm?courtdate=${cas.date}"> <u> &nbsp;<c:out value="${cas.courtNumber}" /> &nbsp; </u></a></td>
	    </c:if>
		<td><fmt:formatDate pattern="dd-MMM-yyyy" value="${cas.date}" /></td>
	    <td><c:out value="${cas.caseSerialNumber}" /></td>
	    <td><c:out value="${cas.caseNumber}" /></td>
	    <td><c:out value="${cas.partyName}" /></td>
	    <td><c:out value="${cas.caseAgainst}" /></td>
	    <td><c:out value="${cas.attorneyName}" /></td>
	    <c:if test="${rowcheck == 'true' }" >
		 <td rowspan="<c:out value="${court.cases.size()}" />" ><c:out value="${cas.justiceName}" /></td>
		 <c:set var="rowcheck" value="false" scope="page" />
	    </c:if>
	    <!-- <td><c:out value="${cas.time}" /></td> -->
	  </tr>
	</c:forEach>
 </c:forEach>
</tbody>
</table>  
</div>
<br> </br>

<!-- DOWNLOAD & SAVE -->
 
<!-- <div id="filterForm" class="wpb_row vc_row-fluid">
					
                    		<form action="./">
                    		<input type="hidden" name="command" value="download" class="custom-btn wpb_button wpb_btn-rounded wpb_btn-small" >
                          <input type="submit" value="DOWNLOAD & SAVE" class="custom-btn wpb_button wpb_btn-rounded wpb_btn-small" >	
                           </form>
					</div>
				</div>
 -->		
<!--/ Lower  search -->
 <!-- Archive Search -->
 
   <div id="home_quick_search">
    <div id="filterForm" class="wpb_row vc_row-fluid">
     <div class="col-centered">
      <form action="." name="history">
       <fieldset>
        <h2 class="pull-left">From </h2>
                
         <div class="input">
         <input type="date" name="fromdate"/> 
        </div>
        
        <h2 class="pull-left">To </h2>
        <div class="input">
        <input type="date" name="todate"/> 
        </div>
        
        <div class="pull-right">
           <input type="hidden" name="command" value="history" class="custom-btn wpb_button wpb_btn-rounded wpb_btn-small" >
           <input type="submit" value="HISTORY SEARCH" class="custom-btn wpb_button wpb_btn-rounded wpb_btn-small" >
        </div>
       </fieldset>
      </form>
     </div>
    </div>
   </div>
<!--/ Archive search -->

</body></html>
