<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@include file="header.html" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">

  
      <meta charset="utf-8">
      <title>IJM Email List</title>
	  <!-- styles -->
		<link rel="stylesheet" type="text/css" href="css/layerslider.css">
		<link rel="stylesheet" type="text/css" href="css/skin.css">
		<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="css/owl.css">
		<link rel="stylesheet" type="text/css" href="css/jquery.css">
		<link rel="stylesheet" type="text/css" href="css/styles.css">
		<link rel="stylesheet" type="text/css" href="css/color-blue.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link href="css/style_admin.css" rel="stylesheet" type="text/css" />
		 <!-- styles -->
   <style>
table#search_results {
	border : 2px solid black;
	border-spacing: 5px;
    width: 100%;
    background-color: white;
}
table#search_results th {
	border : 2px solid black;
	font: 18px bold;
    color: white;
    background-color: blue;
} 

table#search_results tr {
	border : 2px solid black;
    text-align: center;
} 
table#search_results td {
	border : 2px solid black;
    text-align: center;
}

input[type="text"], textarea {

  background-color : #d2d2d2; 

}
input:focus {
    background-color: yellow;
}

</style>	 
</head>

<body background="images/bg.png">
<!-- logo -->


<div id="tableData">
<table id="search_results" style="width:100%" border="1" background-color: #f1f1c1;>
 <thead>
  <tr>
    <th>No.</th>
    <th>Email ID</th>
    <th>Modify</th>
    <th>Delete</th>
  </tr>
 </thead>
 <tbody>
  <c:set var="count" value="0" scope="page" />
  <c:forEach items="${names}" var="name">
   <tr>
    <c:set var="count" value="${count+1}" />
    <td> <c:out value="${count}" /> </td>
    <!-- <td><c:out value="${name}" /></td> -->
	<form action="/ijm/emails/" name="formnamemodify" method="post">
	<td align='center'><input type="text" name="nameModify" value="<c:out value="${name}" /> "  size=90></td>
	<td>
    <input type="submit" style="font-face: 'Comic Sans MS'; font-size: larger; color: brown; " value="Modify"><input type="hidden" name="command" value="nameModify">
	<input type="hidden" name="nameOriginal" value="<c:out value="${name}" /> ">
	</td>
	</form>
	<td align='center'><form action="/ijm/emails/" name="formnamedelete" method="post">
	<input type="submit" style="font-face: 'Comic Sans MS'; font-size: larger; color: red; " value="Delete"><input type="hidden" name="command" value="nameDelete">
	<input type="hidden" name="nameOriginal" value="<c:out value="${name}" /> "></form></td>
	<!-- <td align='center'></td> -->
  </tr>
  <!-- For adding email -->
  </c:forEach>
   <tr>
    <td> <c:out value="${count+1}" /> </td>
	<form action="/ijm/emails/" name="formnamemodify" method="post">
	<td><input type="text" name="nameAdd" value="" size=90></td>
	<td>
    <input type="submit" style="font-face: 'Comic Sans MS'; font-size: larger; color: green; " value="Add"><input type="hidden" name="command" value="nameAdd">
	</td>
	</form>
	<td align='center'></td>
	<!-- <td align='center'></td> -->
  </tr>
</tbody>
</table>  
</div>

</body></html>
