GRANT ALL PRIVILEGES ON *.* TO busystorm@localhost IDENTIFIED BY 'busystorm' WITH GRANT OPTION;

GRANT ALL PRIVILEGES ON *.* TO eavissa@localhost IDENTIFIED BY 'eavissa' WITH GRANT OPTION;

create database ijm;

use ijm;

CREATE TABLE cases (
	case_date date NOT NULL,
	court_number varchar(10) NOT NULL,   
	case_time varchar(10),
	case_serial_no varchar(5),
	case_number varchar(20) NOT NULL,   
	party_name varchar(100),
	case_against varchar(200),   
	attorney_name varchar(250),
	justice_name varchar(100)
);

CREATE TABLE filtered_cases (
	case_date date NOT NULL,
	court_number varchar(10) NOT NULL,   
	case_time varchar(10),
	case_serial_no varchar(5),
	case_number varchar(20) NOT NULL,   
	party_name varchar(100),
	case_against varchar(200),   
	attorney_name varchar(250),
	justice_name varchar(100)
);

CREATE TABLE `temp_cases` (
  `case_date` date NOT NULL,
  `court_number` varchar(10) NOT NULL,
  `case_time` varchar(10) DEFAULT NULL,
  `case_serial_no` varchar(5) DEFAULT NULL,
  `case_number` varchar(20) NOT NULL,
  `party_name` varchar(100) DEFAULT NULL,
  `case_against` varchar(200) DEFAULT NULL,
  `attorney_name` varchar(250) DEFAULT NULL,
  `justice_name` varchar(100) DEFAULT NULL
);

CREATE TABLE `alias_cases` (
  `case_date` date NOT NULL,
  `court_number` varchar(10) NOT NULL,
  `case_time` varchar(10) DEFAULT NULL,
  `case_serial_no` varchar(5) DEFAULT NULL,
  `case_number` varchar(20) NOT NULL,
  `party_name` varchar(100) DEFAULT NULL,
  `case_against` varchar(200) DEFAULT NULL,
  `attorney_name` varchar(250) DEFAULT NULL,
  `justice_name` varchar(100) DEFAULT NULL
);

CREATE TABLE `court_info`
 (court_number varchar(10) NOT NULL,
 justice_name varchar(100),
 case_date date NOT NULL,
 notes varchar(4000)
);

CREATE TABLE name_list (
	name varchar(100)
);

CREATE TABLE name_list2 (
	name varchar(100)
);

CREATE TABLE name_dipak (
	name varchar(100)
);

CREATE TABLE email (
	name varchar(100)
);

CREATE TABLE `user` (
  `username`  varchar(45) NOT NULL,
  `password`  varchar(245) DEFAULT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `middlename` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`username`)
);

INSERT INTO `filtered_cases`( `case_date`, `court_number`, `case_serial_no`,`case_number`, 
`party_name`,`case_against`,`attorney_name`,`justice_name`) 
  (SELECT a.case_date,
  a.court_number,
  a.case_serial_no,
  a.case_number,
  a.party_name,
  a.case_against,
  a.attorney_name,
  a.justice_name FROM `cases` a, `name_list` b 
  WHERE a.party_name LIKE CONCAT('%',b.name,'%')
  AND a.case_number NOT LIKE '%CRA%'
  AND a.case_number NOT LIKE '%CRM%'
  AND a.case_number NOT LIKE '%CRR%'
  AND a.case_number NOT LIKE '%CRAN%'
  AND a.case_number NOT LIKE '%AST%' );



insert into name_dipak values ('DeepakKrPrahladka');
insert into name_dipak values ('DeepakKr.Prahladka');
insert into name_dipak values ('DipakKr.Prahladka');
insert into name_dipak values ('DipakKr.Prahladka');
insert into name_dipak values ('DipakKumarPrahladka');
insert into name_dipak values ('DKPrahladka');